#!/usr/bin/env python3
# Fix Video Link for Linux
# © Drugwash, 2023-2024 GPL v2 or later
#================================================================
#	IDENTITIY
#================================================================
appname = "Fix Video Link for Linux"
appver = "0.28 alpha"
author = "Drugwash"
reldate = "Feb 27, 2024"
#================================================================
#	IMPORT HELPERS
#================================================================
ISPIP = None
def tryInst(cmnd):
	sts = None
	try: sts = subprocess.check_output(cmnd, shell=True)
	except Exception as e: print(e)
	return True if sts == 0 else False
def modInst(pname, mname=None):
	print("• Trying to install missing module, please wait...")
	res = None
	if pname:
		pname = "python3-"+pname
		res = tryInst("pkexec apt-get install -y --fix-missing "+pname)
		if not res and mname and ISPIP:
#			res = tryInst("pkexec python3 -m pip install --user "+mname)
			res = tryInst(sys.executable+" -m pip install --user "+mname)
	elif mname and ISPIP:
#		res = tryInst("pkexec python3 -m pip install --user "+mname)
		res = tryInst(sys.executable+" -m pip install --user "+mname)
	if res: return True
	else:
		m = pname if pname and not mname else \
			mname if mname and not pname else \
			pname+"/"+mname
		print('‣ '+m+' module cannot be installed')
		return False
#================================================================
#	STANDARD IMPORTS
#================================================================
import gi
gi.require_version('Gdk', '3.0')
gi.require_version('Gtk', '3.0')
gi.require_version('GdkPixbuf', '2.0')
from builtins import object
from typing import Union, Tuple, Any
from gi.repository import Gdk, GdkPixbuf, Gio, GLib, GObject, Gtk, Pango
from PIL import Image
from datetime import datetime as dt
import configparser
import logging, os, sys, glob, subprocess, re, threading, time
import html, shutil, shlex, errno, json, base64, ssl
ISPIP = shutil.which("pip")
if not ISPIP: modInst('pip')
try:
	gi.require_version('WebKit2', '4.0')
	from gi.repository import WebKit2 as WebKit
except ValueError:
	sts = modInst('gir1.2-webkit2-4.0')
	if sts:
		gi.require_version('WebKit2', '4.0')
		from gi.repository import WebKit2 as WebKit
	else: raise ImportError('‣ gir1.2-webkit2-4.0 module cannot be installed')
import urllib.request
import webbrowser as browser
import locale
import gettext

_ = gettext.gettext
dl = locale.getdefaultlocale()
localedir = "locale"
tr = {}; langs = ['en_US.UTF-8', 'ro_RO.UTF-8']
for l in langs:
	tr[l] = gettext.translation('FVLL', localedir, languages=[l], fallback=True)
tr['.'.join(dl)].install(names=['gettext', 'ngettext'])
## Initial debug switch, overridden by toggle button
debug = True
#================================================================
#	ADDITIONAL IMPORTS
#================================================================
## Debian-based distros want a different approach:
## sudo apt install python3-<package_name>
try:
	from bs4 import BeautifulSoup as bs
	from bs4.dammit import EntitySubstitution as esub
except ModuleNotFoundError as e:
	res = modInst('beautifulsoup4', 'bs4')
	time.sleep(1) # give the system time to settle
	if res:
		from bs4 import BeautifulSoup as bs
		from bs4.dammit import EntitySubstitution as esub
	else: raise ImportError('‣ bs4 module cannot be installed')

try: import validators as valid
except ModuleNotFoundError as e:
	res = modInst('validators', 'validators')
	time.sleep(1) # this failed to import in Mint 21 after install
	if res: import validators as valid
	else: raise ImportError('‣ validators module cannot be installed')

try: import pyperclip
except ModuleNotFoundError as e:
	res = modInst('pyperclip', 'pyperclip')
	if res: import pyperclip

try: import regex as rx
except ModuleNotFoundError as e:
	res = modInst('regex', 'regex')
	if res: import regex as rx
	else: raise ImportError('‣ regex module cannot be installed')

## imports for version comparison
import platform
try: import packaging.version as version
except ModuleNotFoundError as e:
	res = modInst('packaging')
	if res: import packaging.version as version
	else: raise ImportError('‣ packaging module cannot be installed')

## we need a convertor for ISO8601 Duration strings
try: import isodate
except ModuleNotFoundError as e:
	res = modInst('isodate', 'isodate')
	if res: import isodate
	else: raise ImportError('‣ isodate module cannot be installed')

## Check if we have lxml module installed
try:
	import lxml
	HTMLPARSER = 'lxml'
except ModuleNotFoundError as e:
	res = modInst('lxml', 'lxml')
	if res:
		import lxml
		HTMLPARSER = 'lxml'
	else:
		try:
			import html5lib
			HTMLPARSER = 'html5lib'
		except ModuleNotFoundError as e:
			res = modInst('html5lib', 'html5lib')
			if res:
				import html5lib
				HTMLPARSER = 'html5lib'
			else:
				print("• The lxml module is not installed, html5lib can't be installed - defaulting to 'html.parser'")
				HTMLPARSER = 'html.parser'

## Check if we have yt-dlp installed
YTDLP = shutil.which("yt-dlp")
if not YTDLP: YTDLP = shutil.which("yt-dlp_linux")
print("• Found "+YTDLP if YTDLP else "• yt-dlp not found, download is disabled")
MTYPE = False
if YTDLP:
	try:
		import mimetypes
		mimetypes.init()
		MTYPE = True
	except ModuleNotFoundError:
		res = modInst('mimetypes', 'mimetypes')
		if res:
			import mimetypes
			mimetypes.init()
			MTYPE = True
		else: print("‣ mimetypes module not found, playback is disabled")
##===============================================================
##	DEBUG HELPERS
##===============================================================
#os.environ['GDK_DEBUG'] = 'all'
#os.environ['GTK_DEBUG'] = 'all'
os.environ['G_DEBUG'] = 'fatal-warnings'
#os.environ['G_MESSAGES_DEBUG'] = 'all'
#================================================================
#	GLOBAL VARIABLES
#================================================================
CHANGE = False
CRTDIR = os.path.dirname(os.path.realpath(__file__))
RESDIR = os.path.join(CRTDIR, 'res')
DBGDIR = os.path.join(CRTDIR, '.tmp')
if not os.path.exists(DBGDIR): os.makedirs(DBGDIR)
CONFIG = os.path.join(CRTDIR, 'FVLL_config.ini')
XCC = ['xclip','-selection', 'clipboard']  # 'copy' option
XCP = ['xclip','-selection', 'clipboard', '-o'] # 'paste' option
FIELDS = {"title":"", "site_name":"", "description":"", "image":"", \
	"image:width":"", "image:height":"", "video:width":"", "video:height":""}
QUALITY = {"best A/V mp4":"-f mp4", "best A/V":"-f \"bv+ba/b\"", \
	"best smallest V":"-S \"+res\"", "smallest file":"-S +size,+br", \
	"best clip, best ext":"-S ext", "best clip max 720p":"-S res:720", \
	"max/closest 20MB":"-f \"b\" -S \"filesize:20\""}
QUALTIPS = ["best MP4 audio/video", "best video-only + best audio-only\nor best combined audio/video", \
	"best video with smallest resolution", "smallest video available", "best clip with best extension", \
	"best clip max 720p", "largest clip max 20MB\nor\nsmallest clip over 20MB"]
FLG = re.MULTILINE
FLGS = re.MULTILINE|re.DOTALL
YTNEEDLE = "(?<=[\?&]v=|embed/|youtu\.be/).*?((?=[\s&\?\"'])|$)"
# SUBTITLE LIST
SUBRE1 = "(?<=Available\ssubtitles.*Formats).*"
SUBRE2 = "(?<=Available\sautomatic.*Formats).*"
SUBNO = "has no subtitles"
DUNEEDLE = "(?<=^\[youtube]\sExtracting\sURL:\s).*(?=$)"
#RE_MERGE="(?<=^\[Merger]\sMerging\sformats\sinto\s\").*(?=\".*?$)"
RE_MERGE="(?<=^\[Merger]\sMerging\sformats\sinto\s\").*(?=\"$)"
#RE_DEST="(?<=^\[download]\sDestination:\s).*?(?=$)"
#RE_DEST="(?<=^\[download]\s(Destination:\s)?)/.*?(?=(\shas\salready\sbeen\sdownloaded)?$)"
RE_DEST="(?<=^\[download]\s(Destination:\s)?)(/.*?)(?=(\shas\salready\sbeen\sdownloaded)?$)"
#RE_SIZE="(?<=\[download]\s100%\sof)\s+\d.*(?=\sin\s.*)"
RE_SIZE="(?<=^\[download]\s100%\sof\s+)(\d.*?)(?=(\sin\s.*)?$)"
## To retain scrolling in WebView use '-webkit-scrollbar{display:none;}' and
## 'body {overflow: scroll;}'
## instead of 'body {overflow: hidden;}'
TESTHTML1 = \
"""
<!DOCTYPE html><html><head><meta charset=\"UTF-8\">
<title>URL testing</title>
<meta name=\"viewport\" content=\"initial-scale=1.0, width=device-width\" />
</head><body><style>body {overflow: hidden;}
a, a:-webkit-any-link, a:visited {text-decoration: underline;color: blue;}
a:hover, a:active {color: orange;}</style><div>
"""
TESTHTML2 = "</div></body></html>"
#================================================================
#	CONFIGURATION
#================================================================
# https://docs.python.org/3.6/library/logging.html#logrecord-attributes
logging.basicConfig(format="%(module)s => %(funcName)s(), line %(lineno)d:%(message)s", \
	level=logging.DEBUG, stream=sys.stderr)
logging.getLogger("urllib").setLevel(logging.DEBUG)
DBGURL = ""
TESTFILE1 = ""
TESTFILE = TESTFILE1
TMPHTMLPAGE = os.path.join(DBGDIR, "videopage.html.txt")
TMPWPPAGE = os.path.join(DBGDIR, "wppage.html.txt")
SUBFILE = os.path.join(DBGDIR, "subtitles.txt")
LATEST = os.path.join(DBGDIR, "debug_feedback.txt")
UAFB1 = "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/121.0"
UAFB2 = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 "+\
	"(KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36"
#================================================================
appcfg = configparser.RawConfigParser(
	allow_no_value=True, inline_comment_prefixes=(';',))
appcfg.optionxform = str
d = GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_DOWNLOAD)
if not os.path.isfile(CONFIG):
	print("there is no config file")
	tf = os.path.join(d, "video", "wp-1700228879093_mp4_std.mp4")
	appcfg['Preferences'] = {
		'USERAGENT':UAFB1,
		'ERRIMG':'image-missing',
		'dest':d,
		'TESTFILE2':tf,
		'SCROLLSTEP':'10',
		'BLINKFREQ':'500',
		'align':'none',
		'quality':'-f mp4',
		'lastsub':'None',
		'clrActive':'#FFC0E8',
		'clrInactive':'#EEEEEC',
		'useDefClrs':'True',
		'codeMode':'False',
		'codeHttps':'True',
		'codeBold':'False',
		'codeServer':'False',
		'codeOwner':'False',
		'codeRuntime':'False',
		'codeImg':'0',
		'codeImgW':'100',
		'codeImgH':'100',
#		'':'',
		}
	with open(CONFIG, 'w') as cf:
		appcfg.write(cf)
		cf.close()
else: appcfg.read(CONFIG)
PROXYHTTP = "" #
PROXYHTTPS = "" #
USERAGENT = appcfg.get("Preferences", "USERAGENT", fallback=UAFB1)
ERRIMG = appcfg.get("Preferences", "ERRIMG", fallback="image-missing")
dest = appcfg.get("Preferences", "dest", fallback=d)
SCROLLSTEP = appcfg.getint("Preferences", "SCROLLSTEP", fallback=10)
BLINKFREQ = appcfg.getint("Preferences", "BLINKFREQ", fallback=500)
align = appcfg.get("Preferences", "align", fallback="none")
TESTFILE2 = appcfg.get("Preferences", "TESTFILE2", fallback="")
debug = appcfg.getboolean("Preferences", "debug", fallback=False)
#================================================================
# Compare versions
def isMinVer(crtv, minv) -> bool:
	"""Check min Python version and return true/false"""
	return (version.parse(crtv) >= version.parse(minv))
if not isMinVer(platform.python_version(), "3.11"):
	GObject.threads_init()  # No longer needed since Python 3.11
#================================================================
#	TRANSLATABLE STRINGS
#================================================================
CLICKHINT = _("click image to open video page on")
#================================================================
#	TOOLTIPS
#================================================================
tt_align = _("Select image alignment\nfor 'image mode' code\n\ncurrently:")
tt_szstep = _("Choose sizing step [px]\nfor large preview image\n\ncurrently:")
tt_clrbck = _("Select window background color\nfor inactive (backdrop) state\n\ncurrently:")
tt_clract = _("Select window background color\nfor active (focused) state\n\ncurrently:")
tt_debug = _("Toggle 'debug' switch on/off\n\ncurrently:")
tt_quality = _("Select max video/audio quality\n\ncurrently:")
tt_subtitle = _("Select subtitle language(s) to download with the video\n\ncurrently:")
tt_outDir = _("Select destination path for downloading video files\n\ncurrently:")
tt_prvw = _("Alt+drag to move window.\n[Wheel scroll to resize it.]\nRight-click image to hide it.")
#================================================================
#	WINDOW GUI
#================================================================
MainGlade = Gtk.Builder()
MainGlade.add_from_file(os.path.join(RESDIR,"FVLL.glade"))

class FVLL(object):
	def __init__(self):
		global debug, DBGURL, TESTFILE, TESTFILE1, TESTFILE2
		global IMGICON, IMGDOWN, IMGNOIM
		def on_wndMain_destroy(widget, data=None):
			if CHANGE:
				with open(CONFIG, 'w') as cf:
					appcfg.write(cf)
					cf.close()
			Gtk.main_quit()
		def on_btnPaste_clicked(widget, data=None, d1=None, d2=None):
			self.paste()
		def on_btnFetch_clicked(widget, data=None):
			self.fetch()
		def on_btnUpdate_clicked(widget, data=None):
			self.update()
		def on_btnCopyCode_clicked(widget, data=None):
			self.cpyCode(self.outCode, True)
		def on_btnDownload_clicked(widget, data=None):
			self.download(self.newurl, self.outPath, self.qual)
		def on_btnDirOK_activate(widget, data=None):
			self.okDir()
		def on_btnDirCancel_activate(widget, data=None):
			self.closeDir()
		def on_outDir_activate(widget, data=None):
			self.selDir()
		def on_btnCopy_clicked(widget, data=None):
			self.cpyCode(self.outService)
		def on_btnGo_clicked(widget, data=None):
			self.openSrv()
		def on_btnClrDesc_clicked(widget, data=None):
			self.clr(self.infoDesc)
		def on_type_group_changed(widget, data=None):
			self.type()
		def on_chkHttps_toggled(widget, data=None):
			self.https()
		def on_chkBold_toggled(widget, data=None):
			self.asmCode()
		def on_chkServer_toggled(widget, data=None):
			self.asmCode()
		def on_chkOwner_toggled(widget, data=None):
			self.asmCode()
		def on_chkRuntime_toggled(widget, data=None):
			self.asmCode()
		def on_img_value_changed(widget, data=None):
			self.img()
		def on_img_group_changed(widget, data=None):
			self.img()
		def on_inUrl_icon_release(widget, icon, event, data=None):
			self.icn(widget)
		def on_infoTitle_icon_release(widget, icon, event, data=None):
			self.icn(widget)
		def on_infoCaption_icon_release(widget, icon, event, data=None):
			self.icn(widget)
		def on_preview_scroll_event(widget, data=None):
			self.rsz(widget, data)
		def on_preview_click_event(widget, data=None):
			self.prvw(widget, data)
		def on_qmenu_selection_done(widget, data=None):
			self.selQMenu(widget, data)
		def on_submenu_selection_done(widget, data=None):
			self.selSub(widget, data)
		def on_btnRld_clicked(widget, data=None):
			self.reload(widget)
		def on_btnDbg_toggled(widget, data=None):
			self.tglDbg(widget)
		def on_btnTfile_toggled(widget, data=None):
			self.tglTfile(widget)
		def on_btnClrBk_color_set(widget, data=None):
			self.chgClr(widget)
		def on_imgAlgn_toggled(widget, data=None):
			self.setAlign(widget)
		def on_pvwStep_value_changed(widget, data=None):
#			self.setStep(widget)
			pass
		def on_szStep_value_changed(widget, data=None):
			self.setStep(widget)
		def on_dUrl_icon_release(widget, icon, event, data=None):
			self.icn(widget, icon)
		def on_btn_resize(widget, data):
#			print(data.width, data.height)
#			widget.set_size_request(data.width, data.height)
			pass

		signals = {
			"on_wndMain_destroy": on_wndMain_destroy,  # wndMain
			"on_btnPaste_clicked": on_btnPaste_clicked,
			"on_btnFetch_clicked": on_btnFetch_clicked,
			"on_btnUpdate_clicked": on_btnUpdate_clicked,
			"on_btnCopyCode_clicked": on_btnCopyCode_clicked,
			"on_btnDownload_clicked": on_btnDownload_clicked,
			"on_outDir_activate": on_outDir_activate,
			"on_btnDirOK_activate": on_btnDirOK_activate,
			"on_btnDirCancel_activate": on_btnDirCancel_activate,
			"on_btnCopy_clicked": on_btnCopy_clicked,
			"on_btnGo_clicked": on_btnGo_clicked,
			"on_btnClrDesc_clicked": on_btnClrDesc_clicked,
			"on_btnExit_clicked": on_wndMain_destroy,  # wndMain
			"on_type_group_changed": on_type_group_changed,
			"on_chkHttps_toggled": on_chkHttps_toggled,
			"on_chkBold_toggled": on_chkBold_toggled,
			"on_chkServer_toggled": on_chkServer_toggled,
			"on_chkOwner_toggled": on_chkOwner_toggled,
			"on_chkRuntime_toggled": on_chkRuntime_toggled,
			"on_img_value_changed": on_img_value_changed,
			"on_img_group_changed": on_img_group_changed,
			"on_infoCaption_icon_release": on_infoCaption_icon_release,
			"on_infoTitle_icon_release": on_infoTitle_icon_release,
			"on_inUrl_icon_release": on_inUrl_icon_release,
			"on_preview_scroll_event": on_preview_scroll_event,
			"on_preview_click_event": on_preview_click_event,
			"on_qmenu_selection_done": on_qmenu_selection_done,
			"on_submenu_selection_done": on_submenu_selection_done,
			"on_btnRld_clicked": on_btnRld_clicked,
			"on_btnDbg_toggled": on_btnDbg_toggled,
			"on_btnTfile_toggled": on_btnTfile_toggled,
			"on_btnClrBk_color_set": on_btnClrBk_color_set,
			"on_imgAlgn_toggled": on_imgAlgn_toggled,
			"on_pvwStep_value_changed": on_pvwStep_value_changed,
			"on_szStep_value_changed": on_szStep_value_changed,
			"on_dUrl_icon_release": on_dUrl_icon_release,
			"resize": on_btn_resize
		}
#		locale.bindtextdomain('fvll', '%s/share/locale' % sys.prefix)
#		gettext.bindtextdomain('fvll', '%s/share/locale' % sys.prefix)
#		gettext.textdomain('fvll')
		self.wndMain = MainGlade.get_object("wndMain")
		self.Preview = MainGlade.get_object("preview")
		self.eventMain = MainGlade.get_object("eventMain")
		self.eventPre = MainGlade.get_object("eventPre")
		## get all widgets
		self.btnPaste = MainGlade.get_object("btnPaste")
		self.btnFetch = MainGlade.get_object("btnFetch")
		self.btnUpdate = MainGlade.get_object("btnUpdate")
		self.btnCopyCode = MainGlade.get_object("btnCopyCode")
		self.btnDownload = MainGlade.get_object("btnDownload")
		self.outDir = MainGlade.get_object("outDir")
		self.chooseDir = MainGlade.get_object("chooseDir")
		self.btnCopy = MainGlade.get_object("btnCopy")
		self.btnGo = MainGlade.get_object("btnGo")
		self.btnClrDesc = MainGlade.get_object("btnClrDesc")
		self.btnExit = MainGlade.get_object("btnExit")
		self.txtGrid = MainGlade.get_object("txtGrid")
		self.imgGrid = MainGlade.get_object("imgGrid")
		self.type0 = MainGlade.get_object("type0")
		self.type1 = MainGlade.get_object("type1")
		self.chkHttps = MainGlade.get_object("chkHttps")
		self.chkBold = MainGlade.get_object("chkBold")
		self.chkServer = MainGlade.get_object("chkServer")
		self.chkOwner = MainGlade.get_object("chkOwner")
		self.chkRuntime = MainGlade.get_object("chkRuntime")
		self.imgSz1 = MainGlade.get_object("size1")
		self.imgSz2 = MainGlade.get_object("size2")
		self.imgSz3 = MainGlade.get_object("size3")
		self.imgSz4 = MainGlade.get_object("size4")
		self.imgW = MainGlade.get_object("imgW")
		self.imgH = MainGlade.get_object("imgH")
		self.imgWidth = MainGlade.get_object("imgWidth")
		self.imgHeight = MainGlade.get_object("imgHeight")
		self.inUrl = MainGlade.get_object("inUrl")
		self.infoTitle = MainGlade.get_object("infoTitle")
		self.infoDesc = MainGlade.get_object("infoDesc")
		self.infoCaption = MainGlade.get_object("infoCaption")
		self.outTest = MainGlade.get_object("outTest")
		self.outCode = MainGlade.get_object("outCode")
		self.outService = MainGlade.get_object("outService")
		self.lblServer = MainGlade.get_object("lblServer")
		self.lblOwner = MainGlade.get_object("lblOwner")
		self.lblRuntime = MainGlade.get_object("lblRuntime")
		self.lblSize = MainGlade.get_object("lblSize")
		self.lblAdded = MainGlade.get_object("lblAdded")
		self.imgThumb = MainGlade.get_object("imgThumb")
		self.imgPreview = MainGlade.get_object("imgPreview")
		self.btnQmenu = MainGlade.get_object("btnQmenu")
		self.qmenu = MainGlade.get_object("qmenu")
		self.qitem1 = MainGlade.get_object("qitem1")
		self.qitem2 = MainGlade.get_object("qitem2")
		self.qitem3 = MainGlade.get_object("qitem3")
		self.qitem4 = MainGlade.get_object("qitem4")
		self.qitem5 = MainGlade.get_object("qitem5")
		self.qitem6 = MainGlade.get_object("qitem6")
		self.qitem7 = MainGlade.get_object("qitem7")
		self.btnSubMenu = MainGlade.get_object("btnSubMenu")
		self.submenu = MainGlade.get_object("submenu")
		self.subitem0 = MainGlade.get_object("subitem0")
		self.btnPmenu = MainGlade.get_object("btnPmenu")
		self.btnRld = MainGlade.get_object("btnRld")
		self.btnDbg = MainGlade.get_object("btnDbg")
		self.btnTfile = MainGlade.get_object("btnTfile")
		self.btnClrAct = MainGlade.get_object("btnClrAct")
		self.btnClrBk = MainGlade.get_object("btnClrBk")
		self.btnStep = MainGlade.get_object("btnStep")
		self.btnAlign = MainGlade.get_object("btnAlign")
		self.pmenu = MainGlade.get_object("pmenu")
		self.imgAlgn1 = MainGlade.get_object("imgAlgn1")
		self.imgAlgn2 = MainGlade.get_object("imgAlgn2")
		self.imgAlgn3 = MainGlade.get_object("imgAlgn3")
		self.imgAlgn4 = MainGlade.get_object("imgAlgn4")
		self.pvwStep = MainGlade.get_object("pvwStep")
		self.errDlg = MainGlade.get_object("errDlg")
		self.dUrl = MainGlade.get_object("dUrl")
		self.browser = MainGlade.get_object("browser")
		self.btnFetch.set_sensitive(False)
		self.btnClrDesc.set_sensitive(False)
		self.btnUpdate.set_sensitive(False)
		self.btnCopyCode.set_sensitive(False)
		self.btnCopy.set_sensitive(False)
		self.btnGo.set_sensitive(False)
		self.qmenu.set_sensitive(True if YTDLP else False)
		self.submenu.set_sensitive(True if YTDLP else False)
		self.outDir.set_sensitive(True if YTDLP else False)
		self.btnDownload.set_sensitive(False)
		self.btnPmenu.set_sensitive(False)
		self.codeW = self.codeH = self.videoimg = ""
		self.outPath = dest
		self.qual = appcfg.get("Preferences", "quality", fallback="-f mp4")
		self.sublangs = ""
		self.hasSubs = self.hasCaps = False
		self.lastSub = appcfg.get("Preferences", "lastsub", fallback="None")
		self.chkHttps.set_active(appcfg.getboolean("Preferences", "codeHttps", fallback=True))
		self.chkBold.set_active(appcfg.getboolean("Preferences", "codeBold", fallback=False))
		self.chkServer.set_active(appcfg.getboolean("Preferences", "codeServer", fallback=False))
		self.chkOwner.set_active(appcfg.getboolean("Preferences", "codeOwner", fallback=False))
		self.chkRuntime.set_active(appcfg.getboolean("Preferences", "codeRuntime", fallback=False))
		self.initImg(appcfg.getint("Preferences", "codeImg", fallback=0))
		self.imgW.set_value(appcfg.getint("Preferences", "codeImgW", fallback=100))
		self.imgH.set_value(appcfg.getint("Preferences", "codeImgH", fallback=100))
		self.type1.set_active(appcfg.getboolean("Preferences", "codeMode", fallback=False))
		self.useDefClrs = appcfg.getboolean("Preferences", "useDefClrs", fallback=True)
		self.proto = "https" if self.chkHttps.get_active() else "http"
		self.idx = 0 # debug only for cursor type

		self.btnDbg.set_active(debug)
		self.btnPaste.get_child().set_markup("<b>Paste URL</b>")
		self.btnFetch.get_child().set_markup("<b>Fetch</b>")
		self.btnCopyCode.get_child().set_markup("<b>Copy code</b>")
		self.btnExit.get_child().set_markup("<b>Exit</b>")
		self.btnAlign.set_tooltip_markup(tt_align+" <b>"+align+"</b>")
		self.btnStep.set_tooltip_markup(tt_szstep+" <b>"+str(SCROLLSTEP)+" px</b>")
		self.btnDbg.set_tooltip_markup(tt_debug+" <b>"+str(debug)+"</b>")
		self.btnSubMenu.set_tooltip_markup(tt_subtitle + " <b>" + self.lastSub + "</b>")
		self.outDir.set_tooltip_markup(tt_outDir + " <b>" + self.outPath + "</b>")

		MainGlade.connect_signals(signals)

		if self.wndMain:
			IMGICON = self.getB64Img(B64ICON)
			IMGDOWN = self.getB64Img(B64DOWN)
			IMGNOIM = self.getB64Img(B64NOIMG)
			self.wndMain.set_title(appname+" v"+appver)
			self.wndMain.set_icon(IMGICON)
			c = appcfg.get("Preferences", "clrActive", fallback="#FFC0E8")
			self.initClr(self.btnClrAct, c)
			c = appcfg.get("Preferences", "clrInactive", fallback="#EEEEEC")
			self.initClr(self.btnClrBk, c)
			self.urlValid = self.urlValid1 if "validators" in sys.modules else self.urlValid2
			self.getAlign(align)
			self.Preview.set_keep_above(True)
			self.webctx = WebKit.WebContext.new_ephemeral()
			self.webctx.set_sandbox_enabled(True)
			self.webctx.set_use_system_appearance_for_scrollbars(False)
			self.web = WebKit.WebView.new_with_context(self.webctx)
			self.scr = self.web.get_screen()
			# SIZING CROSS CROSSHAIR IRON_CROSS TARGET
			cursH = Gdk.Cursor(self.scr.get_display(), Gdk.CursorType.HAND1)
			cursM = Gdk.Cursor(self.scr.get_display(), Gdk.CursorType.DIAMOND_CROSS)
			self.eventPre.get_window().set_cursor(cursM)
			self.Preview.hide()
			self.wvSet()
			self.web.set_editable(False)
			self.web.load_uri("about:blank")
			self.browser.add(self.web)
			self.wndMain.show_all()
			self.type()
			self.buildQMenu()
			self.eventMain.get_window().set_cursor(cursH)
			self.web.get_window().set_cursor(cursH)
			if debug:
				DBGURL, TESTFILE1 = self.dbgUrl(LATEST)
				TESTFILE = TESTFILE2 if self.btnTfile.get_active() else TESTFILE1
			GLib.timeout_add(1000, self.pulse, self.btnPaste)
			Gtk.main()
		else:
			logging.debug("‣ Error in FVLL()")
#================================================================
	def wvSet(self):
		self.wvset = self.web.get_settings()
		self.wvset.set_property('default-font-size', 7)
		self.wvset.set_property('enable-plugins', True)
		self.web.set_settings(self.wvset)
#================================================================
	def getB64Img(self, data):
		raw = base64.b64decode(data)
		dbytes = GLib.Bytes(raw)
		strm = Gio.MemoryInputStream.new_from_bytes(dbytes)
		img = GdkPixbuf.Pixbuf.new_from_stream(strm)
		return img
#================================================================
	def pulse(self, wgt):
		self.pulsating = True
		self.lblstate = True
		txt = wgt.get_child().get_text()
		GLib.timeout_add(BLINKFREQ, self.pulseThread, [wgt, txt])

	def pulseThread(self, obj):
		wgt = obj[0]
		txt = obj[1]
		ba = wgt.get_allocation()
		wgt.set_size_request(ba.width, ba.height)
		self.lblstate = not self.lblstate
		wgt.get_child().set_markup('<b>'+txt+'</b>' if self.lblstate or not self.pulsating else "")
		return True if self.pulsating else False

	def pulseOff(self, wgt, txt):
		self.pulsating = False
		time.sleep(BLINKFREQ/1000 + 0.1)
		wgt.get_child().set_markup('<b>'+txt+'</b>')
#================================================================
	def dbgUrl(self, f) -> Tuple[str, str]:
		global debug
		if not f or not os.path.isfile(f) or os.path.getsize(f) == 0:
			# if not debug: return else: msgbox()
			self.errDlg.run()
			self.errDlg.hide()
			u = self.dUrl.get_text()
			if u and self.paste(u):
				try:
					debug = False
					self.fetch()
					self.download(u, DBGDIR)
					debug = True
					return u, self.filename
				except Exception as e:
					logging.debug("‣ "+repr(e))
			return "",""
		with open(f) as lf:
			r = lf.read()
		if not r: return "",""
		u = t = ""
		try:
			u = re.search(DUNEEDLE, r, flags=FLG).group()
		except Exception as e:
			logging.debug("‣ DUNEEDLE > "+repr(e))
		try:
			t = rx.findall(RE_DEST, r, flags=FLGS)
			if t: t = t[len(t)-1][1]
		except Exception as e:
			logging.debug("‣ RE_DEST > "+repr(e))
		return u, t
#================================================================
#	BTN PASTE: GET VIDEO URL
#================================================================
	def paste(self, inp=None):
		txt = inp if inp else self.pasteClip() if not debug else DBGURL
		self.inUrl.set_text(txt)
		if self.urlValid(txt): # should check for http or valid domains
			self.oProto = re.search("^https?", txt) # get original proto
			self.btnFetch.set_sensitive(True)
			self.pulseOff(self.btnPaste, "Paste URL")
			GLib.timeout_add(100, self.pulse, self.btnFetch)
			return True
		else:
			self.btnsOff()
			return False
#================================================================
	def pasteClip(self):
		txt = ""
		try:
			txt = pyperclip.paste()
		except Exception as e:
			if debug: logging.debug("‣ "+repr(e))
			try:
				p = subprocess.Popen(XCP, stdout=subprocess.PIPE)
				ret = p.wait()
				txt = p.stdout.read().decode()
			except Exception as e:
				if debug: logging.debug("‣ "+repr(e))
				txt = ""
		return txt
#================================================================
	def urlValid1(self, url):
		if valid.url(url):
			return url
		else:
			if re.match("https?://", url):
				return False
			nurl = "https://"+url
			if valid.url(nurl):
				return nurl
			return False
#================================================================
	def urlValid2(self, url):
		if not url: return False
		try:
			urllib.request.urlopen(url)
			return url
		except ValueError:
			if re.match("https?://", url):
				return False
			nurl = "https://"+url
			try:
				urllib.request.urlopen(nurl)
				return nurl
			except ValueError:
				return False
#================================================================
	def btnsOff(self):
		self.btnFetch.set_sensitive(False)
		self.btnUpdate.set_sensitive(False)
		self.btnCopyCode.set_sensitive(False)
		self.btnCopy.set_sensitive(False)
		self.btnGo.set_sensitive(False)
		self.qmenu.set_sensitive(True if YTDLP else False)
		self.outDir.set_sensitive(True if YTDLP else False)
		self.btnDownload.set_sensitive(False)
		self.pmenu.set_sensitive(False)
#================================================================
#	BTN FETCH: DOWNLOAD WEBPAGE
#================================================================
	def fetch(self):
		url = self.inUrl.get_text()
		self.videoimg = ""
		if not self.urlValid(url): return
		self.pulseOff(self.btnFetch, "Fetch")
		self.newurl = self.fixUrl(url)
		# show 'downloading' image
#		self.setImg(self.imgThumb, IMGDOWN, False, True, 0)
		GLib.idle_add(lambda: self.setImg(self.imgThumb, IMGDOWN, False, True, 0))
#		self.btnFetch.set_sensitive(False)
		GLib.idle_add(lambda: self.btnFetch.set_sensitive(False))
		while Gtk.events_pending():
			Gtk.main_iteration()
#		if debug: time.sleep(1.0)
		time.sleep(1.0 if debug else 0.1)
#		GLib.idle_add(lambda: self.doFetch(self.newurl))
#		thread = threading.Thread(target=self.doFetch, args=(cmnd,))
		thread = threading.Thread(target=self.doFetch, args=(self.newurl,))
		thread.daemon = True
		thread.start()

	def doFetch(self, url):
		txt, htxt = self.getPage(url, TMPHTMLPAGE)
		if len(txt) == 0:
			logging.debug("‣ Error, page length is zero")
#			self.setImg(self.imgThumb, ERRIMG, Gtk.IconSize.DIALOG, True, 0)
#			self.btnFetch.set_sensitive(True)
			GLib.idle_add(lambda: self.setImg(self.imgThumb, ERRIMG, Gtk.IconSize.DIALOG, True, 0))
			GLib.idle_add(lambda: self.btnFetch.set_sensitive(True))
		else:
			self.extractData(txt, htxt)
			GLib.idle_add(lambda: self.updateGui())
		return False

	def updateGui(self):
		self.lblServer.set_markup("<b>"+FIELDS["site_name"]+"</b>")
		self.lblOwner.set_markup("<b>"+self.owner+"</b>")
		self.lblRuntime.set_markup("<b>"+self.runtime+"</b>")
		sz = "<b>"+FIELDS["video:width"]+"x"+FIELDS["video:height"]+"</b>" \
			if FIELDS["video:width"] and FIELDS["video:height"] else ""
		self.lblSize.set_markup(sz)
		self.lblAdded.set_markup("<b>"+self.added+"</b>")
		self.infoTitle.set_text(html.unescape(FIELDS["title"]))
		self.infoDesc.get_buffer().set_text(html.unescape(FIELDS["description"]))
		if "youtube.com" in self.newurl:
			self.outService.set_text(self.newurl.replace("youtube.com", "youtubepp.com"))
			self.btnCopy.set_sensitive(True)
			self.btnGo.set_sensitive(True)
		p = FIELDS["image"]
		if p:
			imgfilename = os.path.join(DBGDIR, os.path.basename(p))
			fparts = imgfilename.split("?")
			if 'i.vimeocdn.com' not in p:
				imgfilename = fparts[0]
			else: imgfilename = fparts[0]+'.'+fparts[1].split('=')[1]
			self.videoimg = imgfilename \
				if debug and os.path.isfile(imgfilename) \
				else self.downImg(p, imgfilename) \
				if self.urlValid(p) else ""
			if debug: print("image URL: "+p+"\nimgfilename: "+imgfilename+"\nself.videoimg: "+self.videoimg)
			if self.videoimg and os.path.isfile(self.videoimg):
				self.setImg(self.imgThumb, self.videoimg, False, True, 0)
				self.setPreviewTT(self.infoTitle.get_text())
				self.img() # take any previous size change into account
				# enable click to preview
		else: # set 'no image' image, disable click to preview
			self.setImg(self.imgThumb, IMGNOIM, False, True, 0)
		if YTDLP:
			self.buildSubMenu(self.newurl)
		self.btnUpdate.set_sensitive(True)
		self.btnClrDesc.set_sensitive(True)
		self.asmCode() if self.type0.get_active() else self.asmImg()
		self.btnCopyCode.set_sensitive(True)
		self.qmenu.set_sensitive(True if YTDLP else False)
		self.submenu.set_sensitive(True if YTDLP else False)
		self.outDir.set_sensitive(True if YTDLP else False)
		self.btnDownload.set_sensitive(True if YTDLP else False)
		self.btnFetch.set_sensitive(True)
		if debug:
			r = self.readFile(LATEST)
			if not r: return
			self.filename, self.filesize = self.getFileInfo(r)
			if os.path.isfile(self.filename):
				self.playMenu(self.filename)
			self.outPath = os.path.dirname(self.filename)
			self.outDir.set_tooltip_markup(tt_outDir + " <b>" + self.outPath + "</b>")
			logging.debug("‣ Debug file: "+self.filename+" ("+self.filesize+")")
			logging.debug("‣ Save path: "+self.outPath)
		return False
#================================================================
	def fixUrl(self, url, proto=1):
## needs addition for FB video url/player
		oUrl = vUrl = res = ""
		surl = re.sub("^https?://", "", url) # strip proto
		if "youtu.be" in surl or "youtube.com" in surl:
			try:
				res = rx.search(YTNEEDLE, surl).group(0)
				oUrl = "www.youtube.com/watch?v=" + res
			except Exception:
				oUrl = surl
		elif "facebook.com" in surl:
			oUrl = surl.split('?')[0]
		elif "fb.watch" in surl:
			oUrl = surl
		elif "rumble.com" in surl:
			# we fix the URL after downloading page
			oUrl = surl
		elif "www.tiktok.com" in surl:
			oUrl = surl.split('?')[0]
		elif "odysee.com" in surl:
			if "$/embed/" in surl:
				surl = surl.replace('$/embed/', '')
			oUrl = surl.split('?')[0]
		elif "vimeo.com" in surl:
			oUrl = re.sub(r"player.vimeo.com/video", "vimeo.com", surl).split('?')[0]
		elif "bitchute.com" in surl:
			oUrl = re.sub(r"/embed/", "/video/", surl).split('?')[0]
		elif "twitter.com" in surl:
			oUrl = surl.split('?')[0]
		else: oUrl = surl
		r = "https" if proto == 1 else "http" if proto == 0 \
			else self.oUrl if proto == -1 and self.oUrl else "https"
		fUrl = r+"://"+oUrl
		if debug: logging.debug("• Fixed URL: "+fUrl)
		return fUrl
#================================================================
	def getPage(self, url, pagepath) -> Tuple[str, Any]:
		if debug:
			try:
				pagetxt = self.readFile(pagepath)
				pagehtml = bs(pagetxt, HTMLPARSER)
			except Exception as e:
				logging.debug("‣ Page not yet downloaded for "+url)
				pagetxt, pagehtml = self.downPage(url, pagepath)
				if pagetxt == "":
					return "", None
		else:
			pagetxt, pagehtml = self.downPage(url, pagepath)
			if pagetxt == "":
				return "", None
		return pagetxt, pagehtml
#================================================================
	def readFile(self, f):
		try:
			if not f:
				raise FileNotFoundError(
					errno.ENOENT, os.strerror(errno.ENOENT), f)
		except Exception as e:
			logging.debug("‣ "+repr(e))
		try:
			if not os.path.isfile(f):
				f = os.path.join(DBGDIR, f)
				if not os.path.isfile(f):
					raise FileNotFoundError(
						errno.ENOENT, os.strerror(errno.ENOENT), f)
		except Exception as e:
			logging.debug("‣ "+repr(e))
		try:
			with open(f, "r") as lf:
				txt = lf.read()
				lf.close()
			return txt
		except Exception as e:
			logging.debug("‣ "+repr(e))
		return ""
#================================================================
	def downPage(self, url, pth) -> Tuple[str, object]:
		r = b""
		q = urllib.request.Request(
			url,
			data=None,
			headers={'User-Agent': USERAGENT}
		)
		ctx = ssl.create_default_context()
		ctx.check_hostname = False
		ctx.verify_mode = ssl.CERT_NONE

		try:
## Some assholes block Python user-agent so this won't work:
#			r = urllib.request.urlopen(url).read()
			print("Try direct URL...")
			r = urllib.request.urlopen(q, context=ctx).read()
#			print(r.decode())
		except Exception as e:
			logging.debug("‣ Direct URL failed for "+url)
			logging.debug(e)
			try: ## Try using proxy for domains blocked by ISPs.
				if self.urlValid(PROXYHTTP) and self.urlValid(PROXYHTTPS):
					ps = urllib.request.ProxyHandler(
						{'http': '%s' % PROXYHTTP, 'https': '%s' % PROXYHTTPS})
					opener = urllib.request.build_opener(ps)
					urllib.request.install_opener(opener)
					os.environ['HTTP_PROXY'] = PROXYHTTP
					os.environ['http_proxy'] = PROXYHTTP
					os.environ['HTTPS_PROXY'] = PROXYHTTPS
					os.environ['https_proxy'] = PROXYHTTPS
#					q.set_proxy(PROXYHOST, PROXYHOST.split('/')[0])
#					print("Try URL through proxy "+PROXYHTTPS)
					print("Try URL through proxy "+os.environ['HTTPS_PROXY'])
					r = urllib.request.urlopen(q, context=ctx, timeout=15).read()
#					r = urllib.request.urlopen(q).read()
#					r = opener.open(q).read()
#					print(r.decode())
			except Exception as e:
				logging.debug("‣ Error downloading page for "+url)
				logging.debug(e)
				return "", None
		txt = r.decode()
#		print("chardet"+"="*54)
#		htmltxt = bs(r, HTMLPARSER)
		htmltxt = bs(txt, HTMLPARSER) # YouTube OK
		print("get/parse end"+"="*50)
		with open(pth, "w") as f:
			f.write(txt)
			f.close()
		return txt, htmltxt
#================================================================
	def extractData(self, page, obj):
## fields = title, site_name, description, image,
## image:width, image:height, video:width, video:height
# we should check site domain/name right off the bat and
# then use corresponding function or bail out
		self.owner = self.runtime = self.added = ""
		for i,j in FIELDS.items():
			FIELDS[i] = ""
		isYT = isTiktok = isWP = isOdysee = isVimeo = False
		isYT = True if obj.find_all("meta", attrs={'property':'og:site_name', 'content':'YouTube'}) else False
		isFB = True if obj.find("html", id="facebook") else False
		isRumble = True if re.search("Rumble\(\"play\",", page) else False
		try: isTiktok = True if "TikTok" in obj.title.string else False
		except Exception as e: logging.debug(e)
		isWP = True if obj.find_all("link", attrs={'href':'//wordpress.com', 'rel':'dns-prefetch'}) else False
		isOdysee = True if obj.find_all("meta", attrs={'property':'og:site_name', 'content':'Odysee'}) else False
		isVimeo = True if obj.find_all("meta", attrs={'property':'og:site_name', 'content':'Vimeo'}) else False
		isBitchute = True if obj.find_all("meta", attrs={'property':'og:site_name', 'content':'BitChute'}) else False
		isClicksud = True if obj.find_all("meta", attrs={'property':'og:site_name', 'content':'Clicksud - Lumea ta digitală'}) else False
#		if debug:
		print("• YT:", str(isYT),
			", FB:", str(isFB),
			", Rumble:", str(isRumble),
			", TikTok:", str(isTiktok),
			", WP private:", str(isWP),
			", Odysee:", str(isOdysee),
			", Vimeo", str(isVimeo),
			", BitChute:", str(isBitchute),
			", Clicksud:", str(isClicksud))
		if isYT:
			self.getYT(obj)
		elif isFB:
			self.getFB(page, obj)
		elif isRumble:
			self.getRumble(obj)
		elif isTiktok:
			self.getTiktok(page, obj)
		elif isWP:
			self.getWPblog(obj)
		elif isOdysee:
			self.getOdysee(obj)
		elif isVimeo:
			self.getVimeo(obj)
		elif isBitchute:
			self.getBitchute(obj)
		elif isClicksud:
			self.getClicksud(page, obj)
#================================================================
#	YOUTUBE
#================================================================
	def getYT(self, obj):
		for i,j in FIELDS.items():
			try:
				r = obj.find("meta", property="og:"+i)['content']
				FIELDS[i] = r
				if debug: logging.debug("• "+i.replace(":", "_")+": "+r)
			except Exception as e:
				FIELDS[i] = ""
		try:
			rt = obj.find("meta", itemprop="duration")['content']
			da = obj.find("meta", itemprop="datePublished")['content']
			ow = obj.find("link", itemprop="name")['content']
			self.owner = ow
#			tot = self.convertTime(rt)
#			self.runtime = time.strftime('%H:%M:%S', time.gmtime(tot))
			self.runtime = self.formatTime(rt, True)
			if debug: logging.debug("• runtime: "+self.runtime)
			self.added = self.convDate(da.split("T")[0], '%x')  # %d %b %Y
			if debug: logging.debug("• added: "+self.added)
			if "?" in FIELDS["image"]:
				FIELDS["image"] = FIELDS["image"].split("?")[0]
				if debug: print("• Clean image URL: "+FIELDS["image"])
		except Exception as e:
			if debug: logging.debug("‣ "+repr(e))
#================================================================
#	FACEBOOK
#================================================================
	def getFB(self, page, obj):
		FIELDS["site_name"] = "Facebook"
		try: ## Reels have no ids whatsoever but yt-dlp can download
			FIELDS["title"] = obj.find("meta", property="og:title")['content'].replace("\n", " ")
			FIELDS["description"] = obj.find("meta", property="og:description")['content']
			FIELDS["image"] = html.unescape(obj.find("meta", property="og:image")['content'])
			if "?" in FIELDS["image"]:
				self.imageName = FIELDS["image"].split("?")[0] # pure filename URL
			if debug: print("• Clean image URL: "+FIELDS["image"]+"\n• Pure image URL: "+self.imageName)
			r = re.search("(?<=\|\sBy\s).*(?=\s\|)", FIELDS["title"])
			if r:
				if debug: print(r)
				try: self.owner = r.group()
				except Exception as e: logging.debug(e)
		except Exception as e: print(e)
#================================================================
	def getScript(self, obj):
		try: s = obj.find_all('script')
		except Exception as e: logging.debug(e); return ""
		for script in s:
			try:
				## Full type is 'application/ld+json' but some
				## may not use full type string so we play it safe
				if "json" in script['type']: return script.string.strip()
			except KeyError: pass
		logging.debug("cannot find json script")
		return ""
#================================================================
#	RUMBLE long URL (rumble.com/<title>.html)
#================================================================
	def getRumble(self, obj):
		script = self.getScript(obj)
		## Logic for rumble.com full links
		self.newurl = obj.find("link", rel="canonical")['href']
		vID = obj.find("link", rel="alternate")['href'].rsplit('%2F', 2)[1]
		## bail out if we can't get the JSON script
		if not vID: logging.debug("vID not found"); return
		self.owner = obj.find("div", class_="media-heading-name").string.strip()
		if debug: print("s json:\n"+json.dumps(script, indent=2, sort_keys=False)+"\n===")
		j = json.loads(script)[0]
		if debug:
			print(json.dumps(j, indent=2, sort_keys=False))
			print("• title:",j['name'],"\naspect:",
				j['width'],"x", j['height'],
				"\nimage:", j['thumbnailUrl'])
		FIELDS["title"] = j['name']
		FIELDS["site_name"] = "Rumble"
		FIELDS["image"] = j['thumbnailUrl']
		FIELDS["video:width"] = str(j['width'])
		FIELDS["video:height"] = str(j['height'])
		FIELDS["description"] = j['description']
		self.runtime = self.formatTime(j['duration'], True)
		self.added = self.convDate(j['uploadDate'])
#================================================================
#	TIKTOK
#================================================================
	def getTiktok(self, page, obj):
		s = obj.find("script", id="__UNIVERSAL_DATA_FOR_REHYDRATION__").string
		j = json.loads(s)["__DEFAULT_SCOPE__"]['webapp.video-detail']
		js = j['itemInfo']['itemStruct']
		FIELDS["site_name"] = "TikTok"
		FIELDS["image"] = js['video']['cover']
		FIELDS["video:width"] = str(js['video']['width'])
		FIELDS["video:height"] = str(js['video']['height'])
		FIELDS["title"] = j['shareMeta']['title']
		FIELDS["description"] = j['shareMeta']['desc']
		self.owner = js['author']['uniqueId']
#		self.runtime = time.strftime('%H:%M:%S', time.gmtime(js['video']['duration']))
		self.runtime = self.formatTime(js['video']['duration'])
		self.added = self.convUnix2Utc(js['createTime'])
		if debug: print(json.dumps(j, indent=2, sort_keys=False))
#================================================================
#	WORDPRESS [private blog] (xxxxxx.home.blog)
#================================================================
	def getWPblog(self, obj):
		FIELDS["site_name"] = "Wordpress [private]"
		self.owner = obj.find("span", class_="author vcard").find("a").string
		dt = obj.find("time", class_="entry-date published")['datetime']
		self.added = self.convDate(dt)
## This one can have multiple videos on the page,
## we need to build a list and allow user to select
## but we don't have that infrastructure yet
		videos = obj.find_all("iframe", title="VideoPress Video Player")
		self.videos = []
		for v in videos:
## We need to get the video page first, then retrieve the image and
## the best video URL. But bastards use js, no info available directly.
#			page, obj = self.downPage(v['src'], TMPWPPAGE)
			i = lambda: None
			i.src = v['src'] # https://video.wordpress.com/embed/<whatever>
			i.w = v['width']
			i.h = v['height']
			self.videos.append(i)
			print(repr(i))
		print(repr(self.videos))
#================================================================
#	ODYSEE - regular URL (odysee.com/@<series>:0/<topic>:b)
#================================================================
	def getOdysee(self, obj):
		j = None
		script = self.getScript(obj)
		try:
			j = json.loads(script)
			if debug: print("s json:\n"+json.dumps(j, indent=2, sort_keys=False)+"\n===")
		except Exception as e:
			if debug: logging.debug("• Odysee has no json script.")
		FIELDS["site_name"] = obj.find("meta", property="og:site_name")['content']
		FIELDS["title"] = obj.find("title").string
		FIELDS["image"] = obj.find("meta", property="og:image")['content']
		FIELDS["image:width"] = obj.find("meta", property="og:image:width")['content']
		FIELDS["image:height"] = obj.find("meta", property="og:image:height")['content']
		try: # complete
			FIELDS["description"] = j['description']
		except Exception as e: # incomplete, new lines stripped
			FIELDS["description"] = obj.find("meta", attrs={'name':'description'})['content']
		if not j: return # embedded URLs don't provide the info below
		FIELDS["video:width"] = obj.find("meta", property="og:video:width")['content']
		FIELDS["video:height"] = obj.find("meta", property="og:video:height")['content']
#		self.directUrl = j['contentUrl'] # not implemented script-wide
		try: self.owner = j['author']['name']
		except Exception: pass
		try: rt = j['duration'] # PTxxHxxMxxS
		except Exception: # [seconds]
			rt = obj.find("meta", property="og:video:duration")['content']
#		tot = self.convertTime(rt)
#		self.runtime = time.strftime('%H:%M:%S', time.gmtime(tot))
		self.runtime = self.formatTime(rt, True)
		try: dt = j['uploadDate']
		except Exception: # Y-m-dTH:M:S.msZ
			dt = obj.find("meta", property="og:video:release_date")['content']
		self.added = self.convDate(dt)
#================================================================
#	VIMEO
#================================================================
	def getVimeo(self, obj):
		FIELDS["site_name"] = obj.find("meta", property="og:site_name")['content']
		FIELDS["title"] = obj.find("meta", property="og:title")['content'].replace("\n", " ")
		FIELDS["description"] = obj.find("meta", property="og:description")['content']
		FIELDS["image"] = obj.find("meta", property="og:image")['content']
		FIELDS["image:width"] = obj.find("meta", property="og:image:width")['content']
		FIELDS["image:height"] = obj.find("meta", property="og:image:height")['content']
		dt = obj.find("meta", property="og:updated_time")['content']
		self.added = self.convDate(dt)
		j = None
		script = self.getScript(obj)
		try:
			j = json.loads(script)[0]
			if debug: print("s json:\n"+json.dumps(j, indent=2, sort_keys=False)+"\n===")
		except Exception as e:
			if debug: logging.debug("• Vimeo has no json script.")
		if not j: return # embedded URLs don't provide the info below
		FIELDS["video:width"] = str(j['width'])
		FIELDS["video:height"] = str(j['height'])
		self.added = self.convDate(j['uploadDate'])
#		self.modified = self.convDate(j['dateModified']) # not used yet
		try: self.owner = j['author']['name']
		except Exception: pass
		try: self.runtime = self.formatTime(j['duration'], True)
		except Exception: pass
#================================================================
#	BITCHUTE
#================================================================
	def getBitchute(self, obj):
		FIELDS["site_name"] = obj.find("meta", property="og:site_name")['content']
		FIELDS["title"] = obj.find("meta", property="og:title")['content'].replace("\n", " ")
		FIELDS["description"] = obj.find("meta", property="og:description")['content']
		FIELDS["image"] = obj.find("meta", property="og:image")['content']
		FIELDS["image:width"] = obj.find("meta", property="og:image:width")['content']
		FIELDS["image:height"] = obj.find("meta", property="og:image:height")['content']
		self.added = self.convBCDate(obj.find("div", attrs={"class":"video-publish-date"}).string)
		self.owner = obj.find("a", title="View Creator").string
		self.runtime = obj.find("meta", attrs={'name':'duration'})['content']
		self.directUrl = obj.find("source")['src']
		if debug: print("Direct URL: "+self.directUrl)
		try:
			a = obj.find("div", id="video-description")
			b = a.find("div", attrs={'class':'full hidden'})
			c = b.p.contents
			if debug: print("‣ "+str(c))
			f=""
			for i in c: f = f+i if isinstance(i, str) else f+'&#10;'
			if debug: print(f)
			FIELDS["description"] = f
		except Exception as e: logging.debug(e)
#================================================================
#	CLICKSUD (only for details, yt-dlp doesn't know it)
#================================================================
	def getClicksud(self, page, obj):
#		FIELDS["site_name"] = obj.find("meta", property="og:site_name")['content']
		FIELDS["site_name"] = "Clicksud"  # original name is too long
		FIELDS["title"] = obj.find("meta", property="og:title")['content'].replace("\n", " ")
		FIELDS["description"] = obj.find("meta", property="og:description")['content']
		FIELDS["image"] = obj.find("meta", property="og:image")['content']
		FIELDS["image:width"] = obj.find("meta", property="og:image:width")['content']
		FIELDS["image:height"] = obj.find("meta", property="og:image:height")['content']
		self.added = self.convDate(obj.find("meta", property="article:published_time")['content'])
#		self.modified = self.convDate(obj.find("meta", property="article:modified_time")['content']
		self.owner = obj.find("meta", attrs={"name":"author"})['content']
		rt = rx.search("(?<=<b>Durata:\s?</b>)\d+(?=\smin)", page).group()
		self.runtime = self.formatTime(rt, True, True)
#		script = self.getScript(obj) # nothing useful there
#================================================================
#	MOVIESHDZONE (requires registration so fuck them dead !)
#================================================================
	def getMoviesHDZ(self, obj):
		FIELDS["site_name"] = "MoviesHDZone"
		FIELDS["title"] = obj.find("meta", property="og:title")['content'].replace("\n", " ")
		FIELDS["description"] = obj.find("meta", property="og:description")['content']
		FIELDS["image"] = obj.find("meta", property="og:image")['content']
#================================================================
	def convUnix2Utc(self, strg, fmt='%x'):
		try: return dt.utcfromtimestamp(int(strg)).strftime(fmt)
		except Exception as e: logging.debug("‣ "+repr(e)); return ""
#================================================================
	def convDate(self, strg, fmt='%x'):
		if debug: logging.debug("• Datetime input: "+strg)
		# new format %:z (Python 3.12+)
		try: dobj = dt.strptime(strg, '%Y-%m-%dT%H:%M:%S%:z')
		except ValueError as e:
			try:
				dobj = dt.strptime(strg, '%Y-%m-%dT%H:%M:%S.%f%z')
				if debug: logging.debug("• "+repr(e))
			except ValueError as e:
				dobj = dt.strptime(strg.split("T")[0], '%Y-%m-%d')
				if debug: logging.debug("• "+repr(e))
		except Exception as e:
			logging.debug("‣ "+repr(e))
		return dt.strftime(dobj, fmt)  # %d %b %Y
#================================================================
	def convBCDate(self, strg):
		return re.search("(?<=\son\s).*(?=\.)", strg).group()
		return strg.split(" on ")[1]
#================================================================
	def convertTime(self, strg, toSec=False):
		try: tot = int(strg) if not toSec else int(strg)*60
		except Exception:
			t = isodate.parse_duration(strg)
			tot = t.total_seconds()
#			tot = isodate.time_isoformat(t, 'HH:MM:SS')
		return tot
#================================================================
	def formatTime(self, strg, conv=False, isMin=False):
		if conv: strg = self.convertTime(strg, isMin)
		return time.strftime('%H:%M:%S', time.gmtime(strg))
#================================================================
	def fixJson(self, strg):
		m = rx.search("(,\[.*[^,]|\{)?(?(1)$|(,[^\"\{][^\]]+:.*?)(?=(,|})))", strg).group()
		if debug: print("• Bad JSON data: |"+str(m)+"|")
		return strg.replace(m, '')
#================================================================
	def downImg(self, url, pth):
		try:
			data = urllib.request.urlopen(url).read()
		except Exception as e:
			logging.debug("‣ Error downloading image for "+url+"\n"+repr(e))
			return ""
		with open(pth, "wb") as f:
			f.write(data)
			f.close()
		img=Image.open(pth)
		FIELDS["image:width"] = img.size[0]
		FIELDS["image:height"] = img.size[1]
		return pth
#================================================================
	def setImg(self, wgt, src, stock: Union[bool, int]=False, resize=False, scale=0):
		wgt.clear(); wgt.hide()
		if src == IMGDOWN or src == IMGNOIM:
			wgt.set_from_pixbuf(src)
		else:
			try:
				if not stock:
					wgt.set_from_file(src)
				else:
					wgt.set_from_icon_name(src, stock) # False/Gtk.IconSize.XXX
			except Exception:
				wgt.set_from_icon_name(ERRIMG, Gtk.IconSize.DIALOG)
				logging.debug("‣ Error, cannot find image "+ src)
			if debug: logging.debug("• src="+src+", stock="+str(stock)+", resize="+str(resize))
		while Gtk.events_pending():
			Gtk.main_iteration()
		if not resize:
			wgt.show()
			return
		lp = wgt.get_pixbuf()
		if lp:
			olsz = [lp.get_width(), lp.get_height()]
			self.resizeImg(wgt, lp, olsz, scale)
		else:
			logging.debug("• No pixbuf for src="+src+", stock="+str(stock)+", resize="+str(resize))
		wgt.show()
#================================================================
	def resizeImg(self, wg, pb, osz, scale):
		if not scale:
			al = self.btnUpdate.get_allocation()
			s = max(al.width / osz[0], min(al.width / osz[0], al.height / osz[1]))
			scale = [s, s]	# get scale factor
		pWidth = int(scale[0] * osz[0])		# get proper width
		pHeight = int(scale[1] * osz[1])	# get proper height
		pxb = pb.scale_simple(pWidth, pHeight, GdkPixbuf.InterpType.BILINEAR)
		wg.set_from_pixbuf(pxb)
		return scale
#================================================================
	def asmCode(self):
		if not hasattr(self, 'newurl'): return
		self.newurl = re.sub(r"https?", self.proto, self.newurl)
		nttl = self.pEncHtml(self.infoTitle.get_text())
		buf = self.infoDesc.get_buffer()
		buftxt = buf.get_text(buf.get_start_iter(), buf.get_end_iter(), False)
		ndesc = self.pEncHtml(buftxt if buftxt else self.infoTitle.get_text())
		nsrv = self.lblServer.get_text().replace("YouTube", "YT")
		qent = "'" if "\"" in ndesc else "\""
		e1 = "<b>" if self.chkBold.get_active() else ""
		e2 = "[" + nsrv + "] " if self.chkServer.get_active() and not self.chkOwner.get_active() \
			else "[" + self.lblOwner.get_text() + "] " \
			if self.chkOwner.get_active() and not self.chkServer.get_active() \
			else "[" + nsrv + " -&gt; " + self.lblOwner.get_text() + "] " \
			if self.chkServer.get_active() and self.chkOwner.get_active() else ""
		e3 = "</b>" if self.chkBold.get_active() else ""
		e4 = " (" + self.lblRuntime.get_text() + ")" if self.chkRuntime.get_active() else ""
		fmtTtl = e1+e2+nttl+e3+e4
		code = "<a\n\thref=\""+self.newurl+"\"\n\ttitle="+qent+ndesc \
			+qent+"\n\ttarget=\"_blank\" rel=\"noopener\">\n\t" \
			+fmtTtl+"\n</a>"
		self.outCode.get_buffer().set_text(code)
		self.showTest(code)
		appcfg['Preferences']['codeBold'] = str(self.chkBold.get_active())
		appcfg['Preferences']['codeServer'] = str(self.chkServer.get_active())
		appcfg['Preferences']['codeOwner'] = str(self.chkOwner.get_active())
		appcfg['Preferences']['codeRuntime'] = str(self.chkRuntime.get_active())
		CHANGE = True
#================================================================
	def pEncHtml(self, txt):
#		out=str(esub.substitute_html(txt).replace("\n","&#10;").replace("\t","&#9;").encode('ascii', 'xmlcharrefreplace'))
#		return out[2:len(out)-1]
		if debug:
			out0 = esub.substitute_html(txt)
			out1 = out0.encode('ascii', 'xmlcharrefreplace') # bytes class, don't print
			out2 = out1.decode('ascii', 'backslashreplace')
			out3 = out2.replace("\n", "&#10;")
			out4 = out3.replace("\t", "&#9;")
			out5 = out4.replace("&eacute;", "&#233;")
			out = out5.replace("&", "&amp;")
			logging.debug("\n•  In: "+txt+"\n• Out: "+out+"\nout0: "+out0+"\nout2: "+out2+"\nout3: "+out3+"\nout4: "+out4+"\nout5: "+out5)
		else: out = esub.substitute_html(txt).encode('ascii', 'xmlcharrefreplace').decode('ascii', 'backslashreplace').replace("\n","&#10;").replace("\t","&#9;").replace("&eacute;", "&#233;")
## this makes serious problems!
		return html.unescape(out)
#================================================================
	def showTest(self, txt="", tt=""):
		if not txt:
			self.web.load_uri("about:blank")
			return
		txt = txt.replace("\t","")
		if debug: print(txt)
		blutxt = TESTHTML1+txt+TESTHTML2
		try: self.web.load_html(blutxt, self.newurl)
		except Exception as e:
			logging.debug("‣ "+repr(e))
#================================================================
	def asmImg(self, show=True):
		if not hasattr(self, 'newurl'): return
		title = self.infoTitle.get_text()
		nttl = html.escape(title)
		self.newurl = re.sub(r"https?", self.proto, self.newurl)
		imgurl = re.sub(r"https?", self.proto, FIELDS["image"])
		capt = html.escape(self.infoCaption.get_text())
		iw = "w="+str(self.imgW.get_value_as_int()) if self.codeW else ""
		ih = "h="+str(self.imgH.get_value_as_int()) if self.codeH else ""
		par = "?" if "?" not in imgurl else "&"
		srcsz = (par+iw+("&" if iw and ih else "")+ih) if iw or ih else ""
		c1 = "[caption id=\"\" align=\"align"+align+"\" "+self.codeW+"]\n" if capt else ""
		c2 = capt+"\n[/caption]" if capt else ""
		code = c1+"<a href=\""+self.newurl+"\"\n\ttarget=\"_blank\" rel=\"noopener\">" \
			+"\n\t<img\n\t\t" \
			+"src=\""+imgurl+srcsz+"\"\n\t\t" \
			+"title=\""+nttl+"&#10;&#10;("+CLICKHINT+" "+FIELDS["site_name"]+")\"\n\t\t" \
			+"alt=\""+nttl+"\"\n\t\t" \
			+"border=\"0\""+self.codeW+self.codeH \
			+" class=\"align"+align+"\" />\n</a>"+c2
		self.outCode.get_buffer().set_text(code)
		self.showTest()
		self.setPreviewTT(title)
		if self.videoimg and show: self.Preview.show()
		appcfg['Preferences']['codeImgW'] = str(self.imgW.get_value_as_int())
		appcfg['Preferences']['codeImgH'] = str(self.imgH.get_value_as_int())
		CHANGE = True
#================================================================
	def setPreviewTT(self, ttl):
		self.imgPreview.set_tooltip_markup(ttl+
			"\n\n<span size=\"small\"><i>"+tt_prvw+"</i></span>")
#================================================================
#	BUILD QUALITY MENU
#================================================================
	def buildQMenu(self):
		qitem = Gtk.MenuItem(label="qual") # keep text to avoid error
		l = qitem.get_child()
		l.set_markup("<b>A/V QUALITY:</b>")
		qitem.set_sensitive(False)
		self.qmenu.append(qitem)
		qitem = Gtk.SeparatorMenuItem()
		self.qmenu.append(qitem)
		g = None
		for idx, q in enumerate(QUALITY):
			qv = QUALITY[q]
			qitem = Gtk.RadioMenuItem(label=q, group=g)
			qitem.set_tooltip_text(QUALTIPS[idx])
			qitem.connect('activate', self.selQMenu, qv)
			self.qmenu.append(qitem)
			if self.qual == qv:
				qitem.set_active(True)
				cq = q
			if not g: g = qitem
		self.qmenu.show_all()
		self.btnQmenu.set_tooltip_markup(tt_quality + " <b>" + cq + "</b>")
#================================================================
#	BUILD SUBTITLE LANGUAGES MENU
#================================================================
	def buildSubMenu(self, url):
		if debug:
			if not SUBFILE or not os.path.isfile(SUBFILE): return
			with open(SUBFILE) as lf:
				r = lf.read()
		else:
			cmnd = " ".join((YTDLP, "--list-subs", url))
			try:
				r = subprocess.check_output(cmnd, shell=True).decode()
				with open(SUBFILE, "w") as lf:
					lf.write(r)
					lf.close()
			except (subprocess.CalledProcessError, Exception) as e:
				logging.debug("‣ "+repr(e))
				return
		# delete all previous menu items
		for i in self.submenu.get_children():
			self.submenu.remove(i)
		if debug: print("lastSub="+self.lastSub)
		sitem = Gtk.MenuItem(label="langs") # keep text to avoid error
		l = sitem.get_child()
		sitem.set_sensitive(False)
		self.submenu.append(sitem)
		sitem = Gtk.SeparatorMenuItem()
		self.submenu.append(sitem)
		self.subitem0 = Gtk.RadioMenuItem(label="None")
		if self.lastSub == "None": self.subitem0.set_active(True)
		self.subitem0.connect('activate', self.selSub, "")
		self.submenu.append(self.subitem0)
		self.sublist = {}
		sublist = None
		self.hasSubs = self.hasCaps = False
		# try for subtitles first
		x = rx.search(SUBRE1, r, flags=FLGS)
		if x:
			sublist = x.group()
			self.hasSubs = True
			l.set_markup("<b>SUBTITLES:</b>")
		else: # try for automatic captions
			x = rx.search(SUBRE2, r, flags=FLGS)
			if x:
				sublist = x.group()
				self.hasCaps = True
				l.set_markup("<b>CAPTIONS:</b>")
		if not sublist:
			l.set_markup("<b>TRANSLATIONS:</b>")
			self.submenu.show_all()
			logging.debug("No subtitles, no captions.")
			return
		# parse sublist, add each language code and name to a dict
		slist = sublist.splitlines()
		for line in slist:
			line = re.sub('\s{2,}', '\t', line)
			i = line.split("\t")
			if len(i) < 3: continue
			self.sublist[i[1]] = i[0]
			sitem = Gtk.RadioMenuItem(label=i[1], group=self.subitem0)
			sitem.connect('activate', self.selSub, i[0])
			self.submenu.append(sitem)
			if self.lastSub == i[1]: sitem.set_active(True)
		self.submenu.show_all()
		self.btnSubMenu.set_tooltip_markup(tt_subtitle + " <b>" + self.lastSub + "</b>")
		if debug: print(("Subtitles:" if self.hasSubs else "Captions:")+"\n"+repr(self.sublist))
#================================================================
#	BTN UPDATE
#================================================================
	def update(self):
		mode = self.type1.get_active()
		if not mode:
			self.asmCode()
		else: self.asmImg(self.Preview.is_visible())
#================================================================
#	BTN COPY CODE / COPY
#================================================================
	def cpyCode(self, wgt, tb=False):
		if tb:
			buf = wgt.get_buffer()
			txt = buf.get_text(buf.get_start_iter(), buf.get_end_iter(), False)
			txt = txt.replace("\n", "")
			txt = txt.replace("\t", " ")
		else:
			txt = wgt.get_text()
		self.copy(txt)
#================================================================
	def copy(self, txt):
		try:
			txt = pyperclip.copy(txt)
		except Exception as e:
			logging.debug("‣ "+repr(e))
			try:
				p = subprocess.Popen(XCC, stdin=subprocess.PIPE)
				p.stdin.write(txt.encode())
				p.stdin.close()
				ret = p.wait()
			except Exception as e:
				logging.debug("‣ "+repr(e))
				txt = ""
#================================================================
#	BTN GO (OPEN Y2MATE URL)
#================================================================
	def openSrv(self):
		try:
			browser.open(self.outService.get_text())
		except Exception as e:
			logging.debug("‣ Open Y2Mate URL error: "+repr(e))
#================================================================
#	CODE OPTIONS BOX
#================================================================
	def type(self):
		"""Text / Image selector"""
		mode = self.type1.get_active()
		if not mode:
			self.imgGrid.hide()
			self.txtGrid.show()
			self.asmCode()
		else:
			self.txtGrid.hide()
			self.imgGrid.show()
			self.asmImg()
		appcfg['Preferences']['codeMode'] = str(mode)
		CHANGE = True

	def https(self):
		"""Protocol type selector"""
		global CHANGE
		self.proto = "https" if self.chkHttps.get_active() else "http"
		self.update()
		appcfg['Preferences']['codeHttps'] = str(self.chkHttps.get_active())
		CHANGE = True

	def img(self):
		"""Image size selector"""
		global CHANGE
		if not hasattr(self, 'newurl'): return
		mode = next(i for i in self.imgSz1.get_group() if i.get_active())
		self.imgPreview.clear()
		try:
			self.imgPreview.set_from_file(self.videoimg)
			lp = self.imgPreview.get_pixbuf()
			olsz = [lp.get_width(), lp.get_height()]
		except Exception as e:
			logging.debug(e)
			self.setImg(self.imgThumb, ERRIMG, Gtk.IconSize.DIALOG, True, 0)
			self.imgPreview.clear()
			self.videoimg = ""
			return
		if mode == self.imgSz1:
			self.imgW.set_sensitive(False)
			self.imgH.set_sensitive(False)
			self.codeW = self.codeH = ""
			scale = [1, 1]
			appcfg['Preferences']['codeImg'] = str(0)
		elif mode == self.imgSz2:
			self.imgW.set_sensitive(True)
			self.imgH.set_sensitive(False)
			w = self.imgW.get_value_as_int()
			self.codeW = " width=\""+str(w)+"\""
			self.codeH = ""
			scale = [w/olsz[0], w/olsz[0]]
			appcfg['Preferences']['codeImg'] = str(1)
		elif mode == self.imgSz3:
			self.imgW.set_sensitive(False)
			self.imgH.set_sensitive(True)
			self.codeW = ""
			h = self.imgH.get_value_as_int()
			self.codeH = " height=\""+str(h)+"\""
			scale = [h/olsz[1], h/olsz[1]]
			appcfg['Preferences']['codeImg'] = str(2)
		else:  # self.imgSz4
			self.imgW.set_sensitive(True)
			self.imgH.set_sensitive(True)
			w = self.imgW.get_value_as_int()
			h = self.imgH.get_value_as_int()
			self.codeW = " width=\""+str(w)+"\""
			self.codeH = " height=\""+str(h)+"\""
			scale = [w/olsz[0], h/olsz[1]]
			appcfg['Preferences']['codeImg'] = str(3)
		self.resizeImg(self.imgPreview, lp, olsz, scale)
		if self.type1.get_active(): self.asmImg()
		appcfg['Preferences']['codeImgW'] = str(self.imgW.get_value_as_int())
		appcfg['Preferences']['codeImgH'] = str(self.imgH.get_value_as_int())
		CHANGE = True

	def initImg(self, idx):
		btns = {self.imgSz1, self.imgSz2, self.imgSz3, self.imgSz4}
		for i, b in enumerate(sorted(btns)):
			if i == idx: b.set_active(True); break
#================================================================
#	VARIOUS
#================================================================
	def icn(self, field, icon=None):
		"""Clear text in Edit widget"""
		if field == self.dUrl and icon == Gtk.EntryIconPosition.PRIMARY:
			field.set_text(self.pasteClip())
		elif field == self.dUrl and icon == Gtk.EntryIconPosition.SECONDARY:
			field.set_text("")
#		else:
#			field.set_text("")

	def clr(self, wgt):
		"""Clear text in TextView widget"""
		self.infoDesc.get_buffer().set_text("")

	def prvw(self, wgt, data):
		# data.type == Gdk.EventType.BUTTON_PRESS
		d=data.type
		if d != Gdk.EventType.BUTTON_PRESS:
			print("‣ Weird data type in preview: "+str(d))
			return
		try:
			if debug: print("•", wgt.get_name(), data.button)
			if wgt == self.eventMain:
				if data.button == 1 and self.videoimg:
					self.Preview.show()
				else:
					self.Preview.hide()
			elif wgt == self.eventPre:
				if data.button == 1:
					self.movePreview(data)
				if data.button == 3:
					self.Preview.hide()
		except AttributeError as e:
			logging.debug("‣ Preview event error: "+repr(e))
		except Exception as e:
			logging.debug("‣ Preview event error: "+repr(e))

	def movePreview(self, data):
		self.Preview.begin_move_drag(1, data.x_root, data.y_root, data.time)

	def rsz(self, wgt, data):
		d = data.get_scroll_deltas()[2] if data.direction == Gdk.ScrollDirection.SMOOTH else data.direction
		if not self.imgSz3.get_active():
			ch = self.imgPreview.get_pixbuf().get_height()
#			ch = self.imgH.get_value_as_int()
			self.imgH.set_value(ch)
			self.imgSz3.set_active(True)
			self.imgH.set_value(ch+SCROLLSTEP*d)
		self.imgH.set_value(self.imgH.get_value_as_int()+SCROLLSTEP*d)
# this is only for debug; we have Preview resize on scroll
# Gdk.CursorType.+SIZING +CROSS +CROSSHAIR !DIAMOND_CROSS
# +DOTBOX +DOUBLE_ARROW (size) IRON_CROSS TARGET
		'''
		n = wgt.get_name()
		print(n, d)
		self.idx += int(d)*2
		try:
			cursM = Gdk.Cursor(self.scr.get_display(), self.idx)
			self.eventPre.get_window().set_cursor(cursM)
			print("cursor", self.idx)
		except Exception:
			pass
		'''
#================================================================
#	BTN VIDEO QUALITY SELECTION
#================================================================
	def selQMenu(self, wgt, data):
#		logging.debug(str(wgt)+" | "+data)
		if debug: print("•",wgt,"|",data)
		q = wgt.get_child().get_text()
		self.qual = data
		self.btnQmenu.set_tooltip_markup(tt_quality + " <b>" + q + "</b>")
		appcfg['Preferences']['quality'] = data
		CHANGE = True
		globals().update(locals())
#================================================================
#	BTN SUBTITLES SELECTION
#================================================================
	def selSub(self, wgt, data):
		lng = wgt.get_child().get_text()
		if debug: print("•",wgt,"|",data,"=",lng)
		if not data or (not self.hasSubs and not self.hasCaps):
			self.sublangs = ""
		else:
			stype = "--write-subs" if self.hasSubs else "--write-auto-subs"
			self.sublangs = stype+" --sub-lang en,"+data+" --convert-subs srt"  #
		self.lastSub = lng
		self.btnSubMenu.set_tooltip_markup(tt_subtitle + " <b>" + lng + "</b>")
		appcfg['Preferences']['lastsub'] = self.lastSub
		CHANGE = True
		globals().update(locals())
#================================================================
#	BTN DOWNLOAD PATH SELECTION
#================================================================
	def selDir(self):
		self.chooseDir.set_current_folder(self.outPath)
		self.chooseDir.show_all()

	def okDir(self):
		# get selected dir
		d = self.chooseDir.get_current_folder()
		if not d: return
		self.outPath = d
		self.chooseDir.hide()
		self.outDir.set_tooltip_markup(tt_outDir + " <b>" + self.outPath + "</b>")
		if debug: logging.debug("• outPath=" + self.outPath)
		appcfg['Preferences']['dest'] = self.outPath
		CHANGE = True
		globals().update(locals())

	def closeDir(self):
		self.chooseDir.hide()
#================================================================
#	BTN DOWNLOAD VIDEO
#================================================================
	def download(self, url, path, qual=False):
		self.setImg(self.imgThumb, IMGDOWN, False, True, 0)
		while Gtk.events_pending():
			Gtk.main_iteration()
		if YTDLP:
			params = "--ignore-errors -P \"" + path + "\" " + url
			cmnd = " ".join((YTDLP, self.sublangs, self.qual, params))
		# else try some alternative (youtube-dl, etc)
		thread = threading.Thread(target=self.downThread, args=(cmnd,))
		thread.daemon = True
		thread.start()
#		thread.join()
		if debug: logging.debug("• Thread started for quality "+qual)

	def downThread(self, cmnd):
		r = None
		self.filename = self.filesize = ""
		if debug:
			logging.debug("• "+cmnd)
			r = self.readFile(LATEST)
		else: r = self.getVideo(cmnd)
		GLib.idle_add(lambda: self.parseResult(r))

	def parseResult(self, r):
		# try to parse strings
		if r:
			self.filename, self.filesize = self.getFileInfo(r)
		if debug: time.sleep(1.0)
		try: self.setImg(self.imgThumb, self.videoimg, False, True, 0)
		except Exception: self.setImg(self.imgThumb, IMGNOIM, False, True, 0)
		if debug: logging.debug("• Finished download")
		if not MTYPE: return False
		if debug:
			if TESTFILE and os.path.isfile(TESTFILE):
				self.filename = TESTFILE  # TESTFILE1 TESTFILE2
				self.filesize = self.fmtsize2(TESTFILE)
			else: return False
		if not self.filename: return False
		GLib.idle_add(lambda: self.playMenu(self.filename))

	def playMenu(self, fname):
		mt = mimetypes.guess_type(fname)[0]
		if self.buildPmenu(mt, fname):
			self.btnPmenu.set_sensitive(True)
		else: self.btnPmenu.set_sensitive(False)
		if debug: logging.debug("• Finished player menu")
		return False
#================================================================
	def getFileInfo(self, r) -> Tuple[str, str]:
		x = y = z = ""
		# [Merger] Merging formats into "<filename>"
		x = re.search(RE_MERGE, r, flags=FLG)
		if x: x = x.group()
		# [download] Destination: <filename>
		y = rx.findall(RE_DEST, r, flags=FLGS)
		if y: y = y[len(y)-1][1]
		# [download] xxx% of  yyy in zz:zz:zz
		z = rx.findall(RE_SIZE, r, flags=FLGS)
		if z: z = z[len(z)-1][0]
		if debug: print("sz="+str(z))
		filename = x if x else y if y else ""
		filesize = z if z and not x else self.fmtsize2(filename) if os.path.isfile(filename) else ""
		print("x=",x,"y=",y,"z=",z,"filename=",filename,"filesize=",filesize)
		return filename, filesize
#================================================================
	def getVideo(self, cmnd):
		try:
			r = subprocess.check_output(cmnd, shell=True).decode()
			if debug: logging.debug("\n"+r)
			with open(LATEST, "w") as lf:
				lf.write(r)
				lf.close()
		except (subprocess.CalledProcessError, Exception) as e:
			logging.debug("‣ "+repr(e))
			r = None
		return r
#================================================================
	def fmtsize2(self, f):
		if not f: return "unknown size"
		fsz = f if isinstance(f, int) else os.path.getsize(f) if os.path.isfile(f) else 0
		n = 0
		exp = {0: 'B', 1: 'kB', 2: 'MB', 3: 'GB', 4: 'TB'}
		while fsz > 1024:
			fsz /= 1024
			n += 1
		return str(round(fsz, 2)) + " " + exp[n]
#================================================================
	def buildPmenu(self, mimetype, filename):
		if not mimetype: return False
		# delete all previous menu items
		for i in self.pmenu.get_children():
			self.pmenu.remove(i)
		pitem = Gtk.MenuItem(label="open") # keep text to avoid error
		l = pitem.get_child()
		l.set_markup("<b>OPEN WITH:</b>")
		pitem.set_sensitive(False)
		self.pmenu.append(pitem)
		pitem = Gtk.SeparatorMenuItem()
		self.pmenu.append(pitem)
		items = 0
		for app in Gio.app_info_get_all_for_type(mimetype):
			dis = False
			l = Gio.DesktopAppInfo.new(app.get_id()) # launcher
			e = l.get_commandline()  # exec + param
			n = l.get_string("Name") # app name
			i = l.get_string("Icon") # icon name
			g = l.get_generic_name() # function (i.e. Media Player)
			if debug: print("•", g, "|", n, "|", e) # gets .desktop files
			if g and g.lower() == "web browser": #continue
				n = n+" (FUCK NO!)"
				dis = True
			if "xplayer" in n.lower() or "xplayer" in e:
				n = n+" (BROKEN!)"
				dis = True
			# build new player menu
			pitem = Gtk.ImageMenuItem(label=n)
			i = Gtk.Image.new_from_icon_name(i, Gtk.IconSize.MENU)
			pitem.set_image(i)
			pitem.set_always_show_image(True)
			if dis: pitem.set_sensitive(False)
#			pitem.connect("activate", self.play, [e.split("%")[0].strip(), filename])
			pitem.connect("activate", self.play, e.split("%")[0]+'"'+filename+'"')
			self.pmenu.append(pitem)
			items += 1
		self.pmenu.show_all()
		fname = os.path.basename(self.filename)
		fpath = os.path.dirname(self.filename)
		mfname = fname.replace("&", "&amp;") # might need html.escape
		mfpath = fpath.replace("&", "&amp;") # might need html.escape
#		tt = ":\n\n"+fname+"\n<b>("+self.filesize+")</b>\n<small>"+fpath+"</small>"
		tt = ":\n\n"+mfname+"\n<b>("+self.filesize+")</b>\n<small>"+mfpath+"</small>"
		self.btnPmenu.set_tooltip_markup("Playback downloaded file"+tt)
		if debug: print("• Found",items, "media players")
		return True if items else False
#================================================================
#	BTN PLAYBACK VIDEO
#================================================================
	def play(self, wgt, cmnd):
#		if debug: logging.debug("playback menu command: "+cmnd[0]+" "+cmnd[1])
		if debug: logging.debug("• Playback menu command: "+cmnd)
		while Gtk.events_pending():
			Gtk.main_iteration()
#		'''
		thread1 = threading.Thread(target=self.playStart, args=(cmnd,))
		thread1.daemon = True
		thread1.start()
#		'''
#		self.playStart(cmnd)
		if debug: logging.debug("• Playback menu exit")
#================================================================
# we really need to place an async command here. See:
# https://stackoverflow.com/questions/636561/how-can-i-run-an-external-command-asynchronously-from-python
	def playStart(self, cmnd):
#		player = cmnd[0]
#		mfile = cmnd[1]
#		if debug: print("playback started:"+player+" "+mfile)
		if debug: print("• Playback started:"+cmnd)
		try:
#			subprocess.check_output("strace -f -e trace=execve "+cmnd, shell=True)
#			subprocess.check_output(cmnd, shell=True)
			subprocess.Popen(shlex.split(cmnd))
		except subprocess.CalledProcessError as e:
			print("‣ subprocess.check_output(shell=True) failed")
			logging.debug(repr(e))
			try:
				subprocess.run(shlex.split(cmnd))
			except subprocess.CalledProcessError as e:
				print("‣ subprocess.run() failed")
				logging.debug(repr(e))
				try:
					subprocess.Popen(shlex.split(cmnd))
				except subprocess.CalledProcessError as e:
					print("‣ subprocess.Popen() failed")
					logging.debug(repr(e))
		except Exception as e:
			logging.debug("‣ Other exception: "+repr(e))
		if debug: logging.debug("• Playback finished")
#================================================================
#	RELOAD SCRIPT
#================================================================
	def reload(self, wgt):
		try:
			sp = os.path.abspath(__file__)
			os.execv(sys.executable, ['python3', sp])
		except Exception:
			print("Can't reload module!")
#================================================================
#	DEBUG STUFF
#================================================================
	def tglDbg(self, wgt):
#		global debug, DBGURL, TESTFILE, TESTFILE1, TESTFILE2
		debug = wgt.get_active()
		self.btnDbg.set_tooltip_markup(tt_debug+" <b>"+str(debug)+"</b>")
		if debug:
			DBGURL, TESTFILE1 = self.dbgUrl(LATEST)
			TESTFILE = TESTFILE2 if self.btnTfile.get_active() \
				and os.path.isfile(TESTFILE2) else \
				TESTFILE1 if os.path.isfile(TESTFILE1) else ""
			logging.debug("• \nDBGURL="+DBGURL+"\nTESTFILE1="+TESTFILE1+
				"\nTESTFILE2="+TESTFILE2+"\nTESTFILE="+TESTFILE+"\ndebug="+str(debug))
		globals().update(locals())
#================================================================
	def tglTfile(self, wgt):
#		global TESTFILE, TESTFILE1, TESTFILE2
		TESTFILE = TESTFILE2 if wgt.get_active() else TESTFILE1
		globals().update(locals())
#================================================================
	def initClr(self, wgt, c):
		color = Hex2RGBA(c)
		wgt.set_rgba(color)
		self.setClr(wgt, color, c)

	def chgClr(self, wgt):
		color = wgt.get_rgba()
		c = RGBA2Hex(color)
		self.setClr(wgt, color, c)

	def setClr(self, wgt, color, c):
		if wgt == self.btnClrAct:
			wgt.set_tooltip_markup(tt_clract+" <b>"+c+"</b>")
			appcfg['Preferences']['clrActive'] = c
			if not self.useDefClrs:
				self.wndMain.override_background_color(Gtk.StateFlags.NORMAL, color)
		elif wgt == self.btnClrBk:
			wgt.set_tooltip_markup(tt_clrbck+" <b>"+c+"</b>")
			appcfg['Preferences']['clrInactive'] = c
			if not self.useDefClrs:
				self.wndMain.override_background_color(Gtk.StateFlags.BACKDROP, color)
				self.wndMain.override_background_color(Gtk.StateFlags.INSENSITIVE, color)
		CHANGE = True
		globals().update(locals())
#================================================================
	def getAlign(self, a):
		aa = {self.imgAlgn1:"none", self.imgAlgn2:"left", self.imgAlgn3:"center", self.imgAlgn4:"right"}
		for w, v in aa.items():
			if a == v: w.set_active(True); break
#================================================================
	def setAlign(self, wgt):
		global align, CHANGE
		if wgt == self.imgAlgn1 and self.imgAlgn1.get_active():
			align = "none"
		elif wgt == self.imgAlgn2 and self.imgAlgn2.get_active():
			align = "left"
		elif wgt == self.imgAlgn3 and self.imgAlgn3.get_active():
			align = "center"
		elif wgt == self.imgAlgn4 and self.imgAlgn4.get_active():
			align = "right"
		if hasattr(self, 'newurl') and self.type1.get_active():
			self.asmImg(self.Preview.is_visible())
		self.btnAlign.set_tooltip_markup(tt_align+" <b>"+align+"</b>")
		if debug: print("• align="+align)
		appcfg['Preferences']['align'] = align
		CHANGE = True
#================================================================
	def setStep(self, wgt):
		SCROLLSTEP = int(wgt.get_value())
		ss = str(SCROLLSTEP)
		self.btnStep.set_tooltip_markup(tt_szstep+" <b>"+ss+" px</b>")
		if debug: print("• step="+ss)
		appcfg['Preferences']['SCROLLSTEP'] = ss
		CHANGE = True
		globals().update(locals())
#================================================================
	def changeUA(self, wgt):
		USERAGENT = wgt.get_text()
		appcfg['Preferences']['USERAGENT'] = USERAGENT
		CHANGE = True
		globals().update(locals())
#================================================================
def RGBA2Hex(rgba):
		crgb = (int(255*rgba.red), int(255*rgba.green), int(255*rgba.blue))
		chex = '#{:02X}{:02X}{:02X}'.format(*crgb)
		if debug: print(rgba.red, rgba.green, rgba.blue,"=>", chex)
		return chex
#================================================================
def Hex2RGBA(chex):
	color = Gdk.RGBA()
	color.parse(chex)
	return color
#================================================================
#	MAIN ICON AND IMAGES
#================================================================
B64ICON = """
iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAIAAADYYG7QAAAACXBIWXMAAC4jAAAuIwF4pT92AAAOqUlEQVR42u2ZeVxTVxbHA4gbIKKIlbpQQXYoiHVmVObTDv1gbbGlQ6e0RYstjgwjIlQIIrZQmwDZSMIStgSKREFAqakiKFtYwjaurTMIhWqoomwS1qzMnPteXsBOcJz/5o95n/N5n/vuu8s3v3POve+9kP75P3aQ/g/0wkDD3O5hcnd/SHt/gLjbV9ztLbrtJbrtXHZ7S1m7jaD95az2l7LEllzxKq7Ygis2Z1SbgSUiM2UgM6FqC3ilGQPaoJbQ3pILfdEINgIYDY3pJYLxYRaYC2aEefUB6Wh8qrt3VN/2EN62F7XbCts3CNrX4SgM8UqCA6Zfnli9jCpaFq+1pYm6MtSju9AGJ1vJQH0Ba10WGs0WjQzjwywwF86kB+gZGnfogwljLRCvyUK/EqFgGixP1E6/JB7Z4viyxeT5BjXaWxgiIkOyUTEsLowGYyKp7MtgFh2TPqD5NMhNWe3rBEgYC0b1inkoOgjjKGSLdBY+V8Zv6eB0WCuoMBomFTBlIfcRTHqAUNw8Q4O5CRfGhDqHouMwCi8zihIahRMWqitDPbpLkM1hISdiUuHum8ekD8hbhMUNQbMap0lEwmAOwkYPR0rgHIahQsOQhS1USwbtoRdgYU5EUpklIqbVBBP4zkOoBwjlFEQxxA3uqTkaEIZMqBKFlNChGIQIDIIEBsHYOUhXhvp5WKEYFi4VGUabY9L6DuJJpA/IuQzlFIriZ2i04YILYxiOTROMTazlyDIInG/zyIKhJdZeJxUWWPOZ1nBRjNvqVQhCZ4NAbIVFsRn1GRokPo4yJ0mWQYAOgksKwG0eWcCcYFhHfJBnmbAYt+LCvHqAMGehDEc5BVEMcYNosFGMQuc5KBBHwQjg7K+jIcwfqw8gsAJ1TixD7kMhhfkOW6gg71ahYNIH9HJWsy2vzVPQ7ilsdM9sdOHUODFrHMBSKu0plXYJ39nF55n8OX91aNGmiG/XhqUvgrn99FqW2cdCm2PfvhTBBSDz/ec2x5ZaRpwxDtPqtIiI8WVkWAtwx+kDeilLmtihnlQ9Z6O5+9nl3jSJZkIl//tUqV0Mg7SHQfLDzJcwP/bS9xqO5M2qNIqeybNLozuPlmlm1OPNg42uuYRORIwvIRy3iqEHCDClCZ3/AejTqub9wsmOIdWI4vaBCirJh7DXCfPJdTvYw2/RKDXSpBsFiz/vjCjTyNXj4sEaFw4eT2V4jONJB4sTJpI+oFVc6Ved6gnV9A9P/xFR07ort3VnrngHr+Y3rJrfMiq30y5u/yZ/1eclLuT+4tsaheYxv5tK8k0k+WD2OmE+Jf4nx9sfa1Sa5jdy040/6IwoQUANg42OGdp4woOJcBwSaQVVH5AFVxoPQEpZ/YBkFx9L8lA8rSA2iYD1Yy8PvPPNJc2UWt49nmTqNw8FM4M3rkbmqUbkCul0/qpDLGP/1oizCKh29JoDB089fH3CRArXRpJZoj4gc4b0eAcCqh2Q7CzE8xxfTjAgyB0/LGh8a4Mzp3vG1Uq10C0KIOJJO3SWvPa9m9zLGqX6SX4Pg+SfbLy7NaIIB6rRAuHrU4h2byHSTQ8Q6CaNk6jHVbK6AckOPp7qv5IHYhbcdHZn3KOKf4BT2oPL4kneZNJvyaTXcMvddbj/+zsw2t8+KaOS/E4Z727SAj26Zs/B1oKAOa8ZzXlNH5BZovR4JwKqeSj5XTaxIuNAgbg8ePyyrD+8lXRlVj37WNCFAeE0XnC+GJI68cOISqEqcj4G4p0yfrMpohABXRu8Zs+EcQigYG26Ia+RFwTqJUsIIL5uUcbWQH9CHgQEM13ex1KPK9UjSpgyiuShsw5uhWZSPX1j9KTB7xNJ3hhQAUr7awPgMhgH21uCCZFQ/mO5RtYHZMrojUVAqkeKobO9D76W9CZI7n3V0BXf0HWioSuupsAqjGqwGwfK33V0sKpXo9GU7vgShIkieYHR7T/6WXRdI9P8EHoV/AghNadQ9SAssMTyHfTrMFoW/zygWc3s7IwGohvKaplSPabCTClypSQZvUXFkpy1OfAm7Xu0A5IrYeJwkms4yb34Tyefdj6U358q3w6UXnqBuAbzw+j5QCbU3hiJWqbSTKvlPZOT7cOTrYOTkmFZy2NZ06BM/Kh0c3SSoRboaxOfC/uSZ+WzY1W/wGU4yTmU5NqSXKLonxqvfZJisRdkewbo6mANiqF/BwrHE21hhWSqma6x+ydbr/sL294tbNoraNqb2/QOWBrsUPi6jKc6b1fodJdMOSTnmn0MCn1h8tpPFe3KJ4q+E214mOsD8v9vFCJcJrs2gGcZFtFBxMaONixdUMNkDIfABwU3YIso35YA06e5BQ133FfcmxHvzsOAdDFUgGWZLu1fPIYgy443YkC/LJBle3RAkEFUy7frwrJnlZofY2u+Nnr9yl84031PRyv682z/gsszl2VaIOYCWRa/QJYBUKxkgXUIgPx1QDjTSYOdGZ4HYOcf7xjMWBzYWyyBHVca3RFrqF21oU0y7rIZFaxD8CSDAb34OvTMSp2NrdQIiGcQwAEj+XNIfnTSHgrJh4Lt6jAffeP7T0TdmmnVd1sSx7uG5D9NSt4q1NFAm2RdDNU+QkBzARSM7a+hz1up4RlAGoet1MReVmQYwjcI4hsG8wyByR+MbrCHbuCLM8GZtvrdG7HnYZn4hX1TNSp/WiEtd4oHT+E0VKTQ3F4GMUTsZUEvtJf9arcvNYosMjxUZBSSZxSUuSiADWbkzzbyoxliTAaIKWWp75md0fAspnoin1Vo+mJb0y0+QuFFPBuxjN/GgZQPZwbSfrx79NLdyCvdX1ztjhb3xTT3xTV3epdUW3y9wG5PPA/J6gZbvPNKF0WeW3QYGvLYldlLg6DAyK9mLXk/1XgvzehtmiHGZOCTahUw1TemVqtVE8qbe8qohn/AxQOjk3yZxns7jhSrppVqlVo5NgPcqkEwhWpIrhqWq0aUj5h3OzbmLPA8tIb7c0TLTPf4yJm+pu3ZpYsjSxZHAkdOWnXmin1s830c8z8xTP/IXPZH1pK9NOPdgEUxejPF5O2uNPFkz9hgaU+FExWnBMOdm2b8Xt3+HNmdJ4qBacXjScXjacUTzIamlfDMNCR/yPqxw7YA3j30PVOvy5K45F7fXd7mlXvJknJuyeHipQiIl1GVZXWAs3Y/c83+1NUf0s3fY5hgWEvfSVnyVsrSd7I3fl7qHV9oezhtuT/NCIHCmW20B1ycuuj97A2ffe9NqX6dXf0Gu/4NXr1PevOb+S2++ZLdBdffKm9xzKhaRdH/TA1vHegV0YJRZUq/uCzmu2UxZ02Qy3J4NenrDyCXnW1grNsPheQLLZQhGRQosqmU8iaGmR/DxA8u6dwLrGXvsJa8C55F7TkX6fk1UEhNq+SZ7eObBbP7RzhDE0KzQ2dMD583jakwibliRm+woDes4TzvvaxqBaXKNKFieUK5aRxSKPtq5it/hQKzRMyw+RQKpxQqGv1cknUg5Xo34rP6IMXyA1TIENFXBtBXBIBnEUf696kWH9J/Gkgan+atPcCtaIfK3JSL+VZHi1cfL1355SXzr66spDSs4rSt4+l7c7UX4m+ugAxMlaYJ5eYYUG5d+pZDUKCViZmOB9HEdTdpdp9R7T5O2vwxusytpGz8CAmWc4mx7hPm2k9Ylh8iIF4Ve91+1oYgKvyAvsdJChWn9jbvlSM5G6NOW5PPW50UrT5Vt5pWb8Vp28B/zru9EASst6BXmieILL6EcTNz69McjiCPnG9muochgvJGqmtwknNwsuM+xHGugboFkVEEl5Nf+ZS14QDb+hOkaE4lxyYk1e5gamkzXCaNjKe6hXNcjmY7RAtt4sqt46vXJteuZbdaF7XZFi3w9cMRfTVrs+bXW3KAXWSJgHLy6jOdjkEh5UILbSuKKuqFxmSPgzS3gwynz9BlaUMyXiioSnYIptvu425Cl6l51awtBzlOYfT2e8jRShVrWyR7a0SOK/m03XHRRkr1y6yW9XmtNvxbjqULfx9yFLbZ8FqteXVr2PALYCABvynTmYyEqWhOeQ0BJVW00D3DWG6HmA5oYnppI90ZuTLpfCPN9SBUMlMvwCVbUMNxCmemX0J9G+9QJmeo0kHO9mPZHjHFjokiG1qDTVrL5sybjkW3PPQp1L1H+z3vln1Rqw2vZX1mtTULxsoXNOe6IiBGhYT5WiRG1sLwCGe7HErfglzJPtfMdg6jjkxQB0bBp6xTxbRHo0ihglqWWzhlYiZl4ClrWwTtbB3y47Vbgq0ni52pVfapDVvSrjsW3nQvurejaoFvjL6I6ZZ70U37IskrmeJN2TDEGUEr3+UEFLgX2lK9otCgF1pT3cO5jmGZtgiIWyLhOiAlgAk5bmKGWXmdolQxC2sZd+5TlGrO1qj0bdHc7XGMHx8kK9X5Xonn3VlVLqmtLvl/8yjs2lF1z7dq4a+wvtXAC0w3nE432uXU2GWet08+60wpdP8y59W4rFdPZr0ak+F2lOd8LHdLVP6mqIINMdmbojLtItMcj0DAgmW4fJEBZ7ej6e5fcD2O8jxieB7xWZ7H87zi+dsS8rcllnjRv/Nk13vmtm7Nv7vzcpfvFWlAw/O+UwMvMN3wPN3pUlznJKhyTBe5MEpcqGecE4UuCQVO8d86HC+0O35mU1zJ+hNF1sdOrycXbIrhb47Js4vOsY/OcYgUOMbynWLznMl5rmS+64nTbl8VuSYI3b4p80gp92RVbMu4to3f+Ju8W96iu7srHwTUS0Pa/sNfC7Ozs/CKo1QqJycnR0dHBwYG+vv779+//zN2QEEqlULl4ODg0NAQnB8+fAg1cKu3t7enp+fevXtdxAFlqOnr64NeDx48gGbQGLqMjY1NT0/DFDARTPdCQLCHKxSKqakpmUz29OlTIBshDriEyomJCSCGM5TxuzjfY+wAYrwANcPDw9AAeo1hB3QBGhgcpnghoPlM8COg5wx2TGMHXlZgB35XLpfr7sIPmMQOHBcOqMF7yYkD7/jvNHD8C26ZeEwo0P8XAAAAAElFTkSuQmCC
"""
B64DOWN = """
iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAS6UlEQVR4nM1ce3AUx5n/za5WWhASGA7Mw5wAEyzANggTzBEQxOHO2CUcXmWOo8rlS0ysuEg4rjDHYQzEbxsuuTuMsSt23RHblVzJYIzJWUbmXSzmaUVEAgQ2rN7o/djV7s7uTN8fmt79ttUzs5JWXL6qrnn1dPf3m+/V3/SMAsAJwGFs+T4AKGSfEgMQAaAB0I2iSerJSOnlNdkYenNNrMN5pCXFOM9I4TzqwnmWgi7QUoySahzzhp1CY4rRiIouEDmQEeO8yICC7sDw9mTXxHqUYV5kYChkH8IxPUfrAjHQnKSkGucYaUMDEEaMX16QAsBllDQAbqMBBTFgeWPM2I8ACKELxLBRVMSkUBcGqQhFBEgGoij5VAooKPRB8MIQ/9B5Hzppj/fJJY4Wt8E77SMCIGjwzXlmADQOoBvAAACDjH3eMJdIHQBjjBUBgKIoc4wGVaMEEZNCziwfoKgmnAEKoCg59DpVowgZPL9G1Y7f4yT3UM3gD5nX5RLHhSiVMfaNwWMu6VsF4CM88LZ0DtIAABmMsUtGB9B1Haqqdly7du2b1atXf1xaWtphdAgAwwiAIQABAmCYAOlEvG3hJoGDyxnjxMHkasWvc/C45FNJ5DykEPA4gFFVQ8zUUKnlEpfGARR4ZIyxAwags8l9XKWjKpxmgOgEgMzMzKcmTJiQ+fbbby+fO3fuDwsKCu6aMmXKu4TRvzIADBtMdRpbDqBqdOQiAPIBcpAoQKKdcpLrvM2I0Q99WDDaHWDwQO05Q+xh0gfAQef8DzTuTQHgSk9Pf8poc4gwzgzyEEKIaReyADwIYD4zCMBPAeSPGTNmE2OMhcNhFcA/k+vr8vLytl++fPlCe3t7cygUCvn9/ubi4uJDkydPzgMwD0BuYWHhbq/Xe7GjoyPQ1NTUVFJScnblypUbAKycPXv2c6dPnz5eU1PTUl9fHyorK7v6ySef/AHAqsWLFz/v8XjOeb3eptra2lBVVVXLyZMn/5idnf04gO8DmAbgAQDTtm7durqtrS1SV1dX7XA4lgH4+4aGhkafzxfKyMhY5XK5ljc3N9eqqhrZsmXLzwDM/+qrr96uqam5qKpqwOfzNZeXl59buXLlCwCe0QwC8EtN0zQWowhjLAJgCoDR6DJ3KQAwAcAMAAsJQPkA1o4bN24LY4z5/X4fgE3k+ovnzp37k67r7MiRIyeys7PXHTp0qCASibBz5879L4A8AD+uqKi4tnbt2i2TJk362f79+z/r6OhgV69e9QL4RVlZmbepqYlt2LDh3REjRqzPz8//z5KSkpsA1hYVFV2sra1lBQUFJydMmLBp7969n9XV1bEjR44cArAAwN8AeBjAbKfTuaCioqKyra2NPf3006/m5ORs7uzsZIFAgD322GMvrV27doeqqqyxsbHa4XAsAfDjmpqaa+vXr9983333rTl48ODnuq6z6urqCgDPEx43u93uf+HHgwYNWjZkyJD5AO4HcI8hkSnU+EY9n8PhwPjx491vvPHGbAA4evSoB/FezDVlypTvAUB+fv6p69evY926dZ6rV6+umDhx4sMA9gJwZGVlvclYV6iUn5/vKS8vf+Luu+/+awAZmZmZIyKRCHJycqb4fL60gwcPVrz33nu/BZCRlZWVHYlEsHHjxtM3b95UtmzZcvaRRx55YuzYsbMNdeN20aFpWuqlS5c88+fPf3LJkiVzhgwZUhkKhcIOh0O5//77xy9atOhexhjOnDlzXtf1AQDYmDFjXmWMOQCkPvPMM566urq80aNHjzVMASdnMBiMxrc+n486xyhxWxF3UdO0dwAora2tDR6P5/iKFSv2ocsuRKu43W43YwxlZWX/2vXAAMYY0tPThwJImzFjxl3btm1bNGnSpEmZmZl3uVyudF3Xoes6AKR+/PHHJ1atWrUwNzd33rx58+Y999xzbVu3bv30wIEDFU6n062qKg4fPryRP9BwOAyXyzXMGAePxxwAnB988MHXc+fOfXL69Ok5LS0tuH79epWiKMrUqVPH5OTkTGOM4Z133rkIIHXmzJmZr7zyyqLJkyffN3jw4KFutzud8EVDqqijIMc0kI7ekAUgE8BgxtgpAMjIyFihqipTVZXGY27G2IcAoCjK836//yW32z0gOzv7V5WVlU0AIoqiAIAaCARCXq/39aFDh96zb9++gx9++GFZWVlZsKSkZLumaRg5cuQ2AI577703PS8vL2vGjBlZc+bMmRUIBIIPPvjgW8XFxRvdbrd74cKFO+vr6zsAhBRFCTgcjtZAINCMLmeiGwCmAcgoKSl5a9SoUVk+n6+9sLDwgsPhwOLFi2cOHjw4s7a21jtx4sRXAQRra2tfHT58+Nj9+/fv27t3b2lxcbFWWVn5K0VRoCjKZsbYawaP6wEEGWO7ATjcbvdjoVCoGUA7gDYAHQCCDsS8ZoAj5fP5WlRVbQYgFhjS1lpeXv5nANi+ffvUBx54wKkoSuDZZ58dfeXKlU0AwgMHDsxkjOHw4cOlJ0+erF20aNFwTdPQZZ8RPHHixFMzZ87M2LNnT/Hnn3/+TTgcRkNDQwsAtbS0tFxVVaxZs2bKuHHjXIqiRJYtWzb2iy++eJE8UBpcq+fPn/domoZBgwZlHj16tOzo0aOlgwYNymSMwePxnDFAD6anp2cyxlBUVPSnoqKiqqVLlw4lfPlI++0A2tvb228DwAsvvHAPYoE0l0QAwHBDCicTA/oQgByhzGaMMV3XGYAly5cv/0VpaamntbW1obOzMxAMBrXW1taWwsLC3wHIe/nll7ffunWrsrGxUaurqwt/+eWXp6qrq5nX62UAfr5nz57Pjx07dqOkpES9cOGCv6CgoHj69OkvAfinWbNmvfbRRx99c+zYsWaPxxM8f/68dvr06bbdu3f/HsDjAB4BkIsub78AwKPz5s1bU1VVpXu9Xl9qauqa1NTUn96+fdvf1tam5ebm/sS479GdO3e+0NjY6A2Hw5qqquFLly4d13Wd87WKYPAEgEfffffdzZ2dnVWMMU3TtA4AY9EVxg0EkKIAGGyoAZ/KKYjFWXSWkGrcxOeKLnTZUB5j8QCY1uexGY/PXIifLSiIn3srwj5NXITRpSUBQRJ4nJmG+ECYSyefhvH6vE8erLuMLa/L71XJPZrRL5/O8RJOQcxQasK+6HFSjAZ4cMxnFtSo0qkbnyalkLouUodO4B1kK86b+Vho8AxSh2ZKVAJAhGzp/JVGHBx82g69n8946AQhQtpmKUJHdABi5sNpXKeSE43GyWD4ORep70I8gCno/iBoe1QCaZTAAeDRAwVYZJ5rBQfPLO1G59wa2RdTV5pQdBgDoQPgT6ZbvIPYkxSljKodTQ1xlXAZ+1zdeTikkHOyjA3d58zwKRil6MSe9ElTTzTtRgFiwr7ZOdqPLmwZZ4AP2C4xKjJHkwWy3BrtXASZMSwttOnPlhR8ugCx+S71zBRMvk9VUpatoVuRxOsMiFcFsxu7j7mLuMOIAwXxEikbIJXeZFCqseXAyTLplEdqM8X0W6IYREmmEj0hMSkqqjOVSO5MXKQkg1yIlyZeeB6Tj4MmemW5yF5RbwE0yyzTIlNpmvlNRXIoFfFqqyHeDoue3SyR2ysgZS+NekMy4MxsI5fCvko/J9oe9eayInu10OfO+0ri001UCpOpwtzLi7GlTPqSCmJfJVAchJUkcvAoiMkgGqjzbaKS12cQ+0OF7dSZSmIyyE51+036gL4BKHo0sV0rNe4vAM3UV8an1fgTpmRJIND96cpANAu0+0Iy4GTgJV36gOSqMN/a2UEKYjJIlpCwUmE63j5TMp2IGYhi6U8JtOvbbMy9pmRIoCwBYCaFooQkg6zatwIxKVJoZsitGrd7yhw0cSuzg8kgMw8sG0dfYkLpTIUCmEgjiQTNImiifaLbZJBV+7Lx0HmxDECzKZ20TorkotWN/DgRCTRzHnfCiSTiTGQAisvfrMCMprPMwDMztlZAiWGK2Xl+LRlk1rbdeByIZXFkDoaCKQNSAcDMmEjEu8pUIxHQ7oQKW4FpZietHI2pnZQ5EfFpyLZ24IlMmDGXDEqkH3FsYp6Q2kVKdLGmjBRRAq1AE5+WlfqaMSFKSzLIqi/ZeGTjVoR9OyGKkpUXtlLd3gAnK8mgnvRNJZBKoWgLOckkMO7YSoX5fiJe1kp17xSAvMiSC/ztoxOxLLXoTAC5qlpmra0CaSvgxDgvUSbEa8kg2n4KrMfAC313wgHkL8hkr3QBEzvI7ZCpjsPa61pJn7j6XcZUMsisXxmoZrbYyiuLOICeN1NhM5eeiPRZAdlfEijrQyziqgLqjcUVELLA2nYqJ6NE7Z+V5MmkgW+TQVagmUkfBZC+G5YJDmDxxo6GElbiajVFM7NzdhKRrIy0Wb9WD9LMDFFVBrpjQM8B6B6Lmem9LAak9sPMgdgxkgyyA83KBstsIOUV6I5HHDlkJ0nlnqqvHXAiQ8kgsV2X5JyZOptJn5kqU1IA89mAWRgjm771FLhkq3BPpM/KI5sBaAWiFEDZjMQugE7UHvaHE5H1wdfe9ET6ZKbKTDujJOq7WawnAuK6hb/bmIX0/L5w/pdAFfC/n4XDryO2plD8pJUG3eJKrrh0lp0tpPvKJBz+j1vwf5REXu44VSOw/3s4/Gv0TG0tvbBV5bgO1C4Qd96Eb39vBv//TbUIHJuKwtdUc3W1VV8gBqC4nNWKop2FAWUqinbchK/PK03vJNUjePFhHN/WJn83YgdcHE52EmiXnUAAYA+g6E0v/MftBv6XQM0IXfshTmypRDBsX9uUoriIACYqhXELsv2AnoMjb3nhP9uHQfU7tUGtzMPXL5ahM0hOi4vMxYXmEOrGkcOkktUqdmlnLdAis3D8zUp0FveCt34nPyINT+LctjNobod8FT4lW+A49USFZcv+41a810NV5+DEjip0XrFp945SAFr7P+LCK4fR0ATJr0skBSbHlBhgDqBdB+L3EtFYqQrB4Fwc31GDzm/7wHPSSIUW+CWKdxSgtgbdxxv3zQe6g2ZLidhAK/BkRfMiFJiPk/9Wh0BFogPpD4pAD29C6a73UfEdHxviv/uVAWonlXFEARQrJSqB4idQGoDIDQR8P8Lpf69HsKZPKPSSdDD9dZT/9jf49gq6zyrEYyvQOElBtFJh8TgRCYwDsQwdbY/i9K5GhOoT5jxJtAs3PtqKK8V0POj+yyrZNC0RMKPkhDxxanZOlta3fF9Sh1DkBBquLMeYaQPgpP8k6Df6b9z69OcoOYXYf23oX5ZooZ+AyYC1dCBAdwApWYGYSIZGMdp3VCMY9qDx6gqMfjANTnePEekBHUB10Spc/BLx4NFCkwayxAH9qY+tFMoAtMpKi8di6sdMKpUKBEMX0FK+FKOnpcGZ1nNo7OkY6j15+PozJgePfu8rA5F+Q2cGXDcQKYCAfVbCThKtjpXv0Bm8jLYbSzBqmguOZH3qBQA4j5biBTj1P1rsa3JR8qgaJ5K2MpO6uHMyAGUkk0C6n0gBAKUc/s5raP/uCYyalgJHUr5WKkP71R/gxO9CYPx3ABxAM/tHP4NN1CPbqjAnmQqL53sqpXFtXYGvowKdNx/HyOkpcPQptX8TvltzcfL9NkT86C59dF8E0M55JBJMMzMA+dZMOq1AtmojSiVob29EyPsoRkx3wtGr9H4dgjXzcWpPNYLtiP8ZBAUyjHjnIf5IUQacmR2U2kDKMEyO6XmrfJlMImXtMQC4gNZmPyJVP8LwHEf3pXaW1IxQ49/i9K6r8DWju9rKHAiVPtn/D8xUOG7M4jF98r3KyPaQxFmOfgYtjQCqczFsRqIg+hBuW4qzv/kaLfWQS50ZgDLPayV9gFyd4+JASn0B0aqu+CTjZjQn0VQ3EM66ORg2Q4Fi2WcQmv8nuLjzEG5XQS55/J8yMvBEx2HnfS3BA+SvFnsDYiL3yObY0cF/hYbqkUir/z6GPAQTEMPQQxtwecd/ofJbWIcqZp7XauZBHyodp4yHKJkZ70QA6Yl0WiUookz8Ebcr7sXA5mkYMlNsQAeLvIlrO9/AjcuQe1kzlTWbeVh5Xdv4j5OV90sGiLJBmWV0dADap6j9NgeD27OR8RC5Vf8At369Hn8+i8SkLlHgrHKBdoACsF8d0B+SKJU+ELv0B1Rfy8WwwHikzwCAg6jb9Q+48BXkts0qSJaBZxc0AxaA9Yb5RJIMiS77TXSdigLAcRbzV0egaz/Aqd8jHvBu+Ud0B8wsXBF/vmMHniWYiUqP1WzFDkRxaYhVkS3w4UzIJFaHHCgrqbOK93oEHpD4CimGeBB5wwrZ58tlxUXadHBmSVgRPHHZnZXayyRSlnVOOnhAz5aY9RZEq/CFf3IQp7roPuOh7ZuBKAJq9s4maeABPV+j11dJ5MVB6jkQ/4ch2epQmfOxAlIW3yUdPKB3ixwTAZGCyY9FAMTstdnqUNqeGYAimLJrSQcPAP4PUnG/EJt25hQAAAAASUVORK5CYII=
"""
B64NOIMG = """
iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAANa0lEQVR4AeyS0QrCMBAETdK01YL4/z9atZp4whbmofSthcANDIrcBm+5i+M4juM4juM4zumEE94OkL9Xc48Kt9/cn68tFxhklEnGraWR4fJfs2AmSL4VMF+UUY7Z4+jMQ0Bx2exlt7E0F2R5H7nOROUz3orKFHOhyFfYRoG4vmyO5tWczAGXU3hla7Eo74Ui/iTlR9mbCZm3+ZSz8kH50mKBSUvezLv50PeMAnll0awob1YZi+az8pM+h/X/I/Nj31xj47iuO/6bmX0sl49dPkhRfFPyQ6ECiaZFyUqiIomtqBZSPeK4UNJIStIobhpANYr2W5UgRRu0QNGmBtqgrVtbSuU6aGKzaeSodRs7TWMF1kNKXElu1FgS9RApvrnkvmemu+j5cHAxAwiF82EF/YA/ZjhzZ+6dM3fvPefcYRZYkrLLch3qHn6tGTAuD9zisus1FEW8XA+v/M4M5SzgVc4fRrDBtxh7TIxYlPslgPpRUt2HWPP4+2j9QCORFsBfoDR7gpkffpmLx8+RuSkvzgYwx8Oa7YE2lo0igVP/FIMf/CqXfgK45nmgBciKAR0g+RKbnthB5x4HO4qiHad7B917trPyiZe58cKvcuqbAHpi+UUZ0f5FGlCNgQ0EUHngUaAT6ABM0kCzGLL1WYZ37qZrr2k8TQQ7+iS9+6tl5dpGqT+qn7UWDGj2wgQBjJLubyW2EmgDTJpE6W5inXvp+wSK40ycXsurz7y3ouo+ikrZfX0kOuTF1SkDWrViQO2zRYAYAcRwnP30PiC9zaQeaKzqKVZtiOEkEN4mc3U7J45dYGn6fEXV/YssXlH3rTvIfZsNAzo1YsAQZ9rgLRYyALvp6gXqMVDuT3IrHetRfJPrp3zIApmqqvtHuX4SxaO0j8r18Vo1oMbH4NvcnAbYTGtzN/EgA0ZF8SEa+1H8CxPviLuyIFqSYwKspv4B6flR5XTfPQY8zJVJD993sKx99HdgAg6iJJE6FBdYmgFyQFaUu8TyHIo6nGRI2Hd3GPAK+eXTzM8DfJyudkzACtnHxUMTcszC5G4yIJD/NjduAYzQ3BRyjVtVFjeHYg2Nzcj4KKq7n8YUijxe1vQBa9aAIeSfZ/xqGc8jGBcoAQU9wwJso3MV0KBcnYZtdKxG8XOWLwFFoKTj4VoyoG+kmUwykxRmTzI3QQAqHs7+K7d/guITdG+wIIm4OdX9vfSNoniNqZMqqVAG3FoyoG+kpYqYwAIw9x1uvU0wS6LMX/HOqSJuHmEtqcExHtnWR6JlkGTrP/PI40M0DSKU8Ap/zqU3gGUVT7s1EwsLvpFmMpkFrGe5cuYrvGdLDMcM0RZFpRsUl44w/sLnGPwswg5WbqqKAF7k+jeukJ8EMiqj49VUD1RjWF56gskUcHua0vX/InMeE5gTzQIzBzj3nTFu/L2LXyaE6rlKmaP7OP0yMC89OKcMSK30QN37stKTTCZl63yf298fIT2MBmaAJZXOWt7Nm0c2knrtSwz98mZatjTitABkKM+cYO4/f58Lx99k4ZoYb0GuL+hJpCbWRFQSoU5my3rZxlXi1JWtbYRaHlCQh8/+PxKqy0BGjX8F/ROu1R7oASW1JuIbqXxHG1D99Is6pS/HsqEpfZm5RQV1vS+qqVU5RxQVRdQDYxjQFqF9QJG5qGTGt6jlgZL2/cxEai0va9pmNsRcrjTOeSLXKKfuFbqs6ZmGu9sW1jW+KvMuLawb++Hc4x73uIcVsDWl8Y2teZ+wMSh8DBS9Cx8PWSHPQVjbQsuHPE9QGyKhHwKJzJlOScAOSSLoimwlS5dTMyZGO8y6zRlWY5vtD3CNPEPmB0u6TttwtVwtbYNIyIdAcdlq380V6dSQ6cP5QFlJO8oRw5EuK5/N1b5eSN1lM79ntl1JrscWeSLTP0RdG1Ey/Uupm4JqA4Cre6A0XIdLJIxwqagUFoaVVEWulIkpSTmKQFZlS3wVAgbVnVflLbk3UiYuqpPr4ohRVH26/aWQ62PqefRLzgO5gF+jpXtgRH3LkgZSsh8DMG5UkBtEjJV/T8WkOWXAhIpfbTmelYDflmt8Fe+mgAaj7iVAv/CybGNAvYq3k0aoZ+nMkA7xVIxdr4wvBsQD8qJlqdtTL8PWPdBWxmjw2fUjDyxCcBjbKTeKu+z6FooMpakOjn2xCIsq/q3/OR85NEDyIeMDog8DlhjTFwOnPk/fw19n5BkUX+DMwb9m/LQaK0uAbXz9lfokPff/Ov0fHCY1Uk8kHcGOVROsGUrTN8n99yvcfv0QF864UBJDJVx2jRGGtBXAYmxYh4u6B6KNCJZjE4402AfqzI+CUsRW/AaDH3iGy2ekosgIqa6q8cyyQMrIVtcBzTvoftQs+1FWPlox4GV5+yD3BhqBlh2sWPMXDD/dQ/I+DBycSAKnvp1E/3qaP/IFBq81c+wg8nLNusIhaoaRdkgSwMREfzRpwhP0fAjoBFYAnV9k1WMhjWxExltRA5AepmkUg4do3iC9rEktKKWA5p2sGHqRjX9kGi+MNLFeVV8Td47pFhEJcE/8ev5pnwe9OXb+IYoEY1+xsRbUzz5GAJtoHmon2jNFacmC6FY6HiaYOpEn92zcQstAN8keDHqo630f6YE3mM+qcSjuQPNf8tBv1xFpUGsipZe4+aPnufrTU8zPtBHjY3R1fZLeDWtpkrYQU2Ozhjhj/wBkLJi2sSpiysG6HfT9tZ6uXURZvHK+IgwK+G5Ojof3VojjRA8wMAK0baX9gV6SreE9WSSz/35jdU3zaQZGgUY15qX307uui7oB9eFmeQ9vvrCHk68f5/bNaYrZt1la/Co/O/9e/v1vfotzf7xIacpw2zQU8SMVWQV8P4fnLuOVFnGLAQbENlfQROIvmch501FWzFMsAjxJz4NA+jP0r1XHDcR1UkPC+2lbhzBDoThJPo/wflrWAU1KqX30fwjFy9w4/RK3rof5jJWx+XIr3z1kBAsmOlXmIbYJSpVFAgoVRQVMTEcywMjHmJz5NXpXDpNuf4im9sdZ0QfwXSamP0VfV0BDY4gLs4pk6/3UDyKcZj5Txve305kAeJDGgQGSbVfIygMQW0dqLYrnGb+o2l8KiDb8svQidUxDmV0fs8A3PIYtugeaY6AyosoGG6jjZancxeAI4xNVAwIcZXQ4RSyGHA8woKOc6/jnGRh2sG2EE8wsu/9nwDaA6rnP0r/+S1z8sdQdbSDSiOLHzMzJs+Cy62uEu2K/q2ZV45xlxtEYUYyPyH63v3l5ldvTE+SyAO+hqRngFrnsvzE1HfoTlu9cPkzHOhSvM7P8g4pQbKV9CEjKNXEMLGwLGd9sLDtMhrN9BwRnuc1kgqNjw5AHjipP3Aqw6PL3mLzyGQaGEF6p/F09joFy3v0UTtM6mlYjFHDdN6rGk/04jgMwTHpVA079Em4OsJYpZ9PEmhAeo23lP3JLh11hJPREaMzChy2YtbAmbJiIYE2GZcZtdEAuPUIUx0TiRTPYN1g8zPhbxs/6LSCDifpHnE/TvyaOE1MzuZNl58NVifHkqZ3oPvpWI3WfZeEyik9V7iPtTCYY+7M4Y1+v6DkMjIUpcxamgE8ejywei7hBGRtLzzZmxiQqCqtUbhbI7A+YuXSL3E2Am5Xtf1T+BmZRmC/uo3Q+GDQWVYXBTlauRnzBw4yfQ7GNjqFfobMLiBfw40X8WEURDPS4H3LcN9topMksFcqF59IMLJFOXZlMA/ODHP89B9tz8SReJY2JDMgOWJtouY87ZDMtqyJQLEPhG4xf/BPWTreRaEN80G+x6ePHmDh/hKvvvMrUYh6XAAqEG7BkpuxMA4pkDDSWCl12HSGAyvEvywx2QM3WQQacK8ASeGWpoyHE2HnA30tffyPReoQ5CpkuvvesDx6ABfYE2w+kiDUAVMo27Kev5W8Z/5kH/tP89O+e4+GnozIExLAju+laXxXhLKoMkPmcnwtJJowY31tbdtAXBWqmQqOOS1qL5RADhskkAyxVMygoLrJ0IY9/rYA/XlV1/yKZC8b/gtwPzAEzR7lx9inO/cEsxQnugAJuFpgVzQU+pxJYTlVBSwURACP7msck/O3ZIQZcALJyv4hKrprMAmykeS2Kc8yfAK6pa5yzLJx4hNaNCCOk1wNfQzyC5xifP8L4bx5k9fon6fmlIRrWJXEabSyngJedpjh5jezlHzJz9k/5n1Pq5ccIJ9yVCfq3VJVba5atmdTMGQlVS3VpT8osi/JSofO/xJzRCsIwDEUZHYL//6nCmKvrpODgcNguioIPZX1ISpNW7e5p3AsFUfJKpXhAo3xP1dhHq9XCKI5EVKUL5kZBtbeKH8Yr/C4Uh/dPGsoqen/axxi1++5Ymd6/nUj6FcGbh8yvtqCM9YHxiQBMx/xOzgSOTHxIMuX58oakX5C4KOnztiuh0gZj7oJZRXpNgaXgq6BSw64JdRsiYARHooS0wzx4DBu1UGuAShP8CmOG33J042v4BmuGBMhGqDJz4sYnv2roZzvGkbBmmhv6ivkUa24/Auv5UtDReCGBemp+sk+A/XOwnoH+v/4Z5NkeHBMAAAAADOnfeiV2AgAAAAAmASz6lkpOQKKrAAAAAElFTkSuQmCC
"""
#================================================================
FVLL()
#================================================================
